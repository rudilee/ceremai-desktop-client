#ifndef REPORTFORM_H
#define REPORTFORM_H

#include <QWidget>
#include <QMap>
#include <QPointer>

#include "types.h"
#include "restmanager.h"

namespace Ui {
    class ReportForm;
}

class ReportForm : public QWidget
{
    Q_OBJECT

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

    void on_showReport_clicked();
    void on_exportReport_clicked();

public:
    explicit ReportForm(StringHash userInfo, QWidget *parent = 0);
    ~ReportForm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::ReportForm *ui;

    StringHash userInfo;

    void setupReportForm();
    void setupCallTrackingTab();
    void setupCampaignSelect(QByteArray data);
    void toggleSearchProgress(bool toggle = true);
    StringHash getTrackingParams();
};

#endif // REPORTFORM_H
