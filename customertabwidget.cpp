#include "customertabwidget.h"
#include "customerdetailform.h"

#include <QLabel>
#include <QVBoxLayout>
#include <QCloseEvent>

CustomerTabWidget::CustomerTabWidget(StringHash userInfo, QWidget *parent) :
    userInfo(userInfo),
    QWidget(parent)
{
    setupSearchForm();

    connect(searchForm, SIGNAL(customerTableClicked(QString,QString)), SLOT(onCustomerTableClicked(QString,QString)));
}

void CustomerTabWidget::closeEvent(QCloseEvent *event)
{
    if (!tabWidget->close()) {
        event->ignore();
    }
}

void CustomerTabWidget::setupSearchForm()
{
    searchForm = new CustomerSearchForm;

    tabWidget = new SearchDetailTab;
    tabWidget->addSearchTab(searchForm, "Customer");

    setLayout(new QVBoxLayout);
    layout()->addWidget(tabWidget);
}

void CustomerTabWidget::onCustomerTableClicked(QString customerId, QString customerCode)
{
    int tabIndex = tabWidget->indexOfLabel(customerCode);
    if (tabIndex != -1) {
        tabWidget->showDetailTab(tabIndex);

        return;
    }

    tabWidget->addDetailTab(new CustomerDetailForm(userInfo, customerId), customerCode);
}
