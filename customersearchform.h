#ifndef CUSTOMERSEARCHFORM_H
#define CUSTOMERSEARCHFORM_H

#include <QWidget>
#include <QMap>
#include <QPointer>

#include "restmanager.h"
#include "searchmodel.h"
#include "searchsortmodel.h"

namespace Ui {
class CustomerSearchForm;
}

class CustomerSearchForm : public QWidget
{
    Q_OBJECT

signals:
    void customerTableClicked(QString customerId, QString customerCode);

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);
    void onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void onModelColumnSorting(QString order, QString sort);

    void on_search_clicked();
    void on_clear_clicked();
    void on_firstPage_clicked();
    void on_previousPage_clicked();
    void on_nextPage_clicked();
    void on_lastPage_clicked();
    void on_pageNumber_editingFinished();
    void on_customerTable_doubleClicked(const QModelIndex &index);

public:
    explicit CustomerSearchForm(QWidget *parent = 0);
    ~CustomerSearchForm();
    
protected:
    void changeEvent(QEvent *e);
    
private:
    Ui::CustomerSearchForm *ui;

    StringHash customerInfo;
    StringHash searchParams;
    QPointer<SearchModel> searchModel;
    QPointer<SearchSortModel> searchSortModel;
    QString selectedId;
    int searchPageNumber;

    void setupCustomerSearch();
    void setupCustomerTable();
    void setupCampaign(QByteArray data);
    void setupCallStatus(QByteArray data);
    void toggleSearchProgress(bool toggle = true);
    void resetNavigationState();
    void retrieveCustomers();
};

#endif // CUSTOMERSEARCHFORM_H
