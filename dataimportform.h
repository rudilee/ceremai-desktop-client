#ifndef DATAIMPORTFORM_H
#define DATAIMPORTFORM_H

#include <QWidget>
#include <QMap>

#include "types.h"

namespace Ui {
    class DataImportForm;
}

class DataImportForm : public QWidget
{
    Q_OBJECT

public:
    explicit DataImportForm(StringHash userInfo, QWidget *parent = 0);
    ~DataImportForm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::DataImportForm *ui;

    StringHash userInfo;
};

#endif // DATAIMPORTFORM_H
