#include "dataimportform.h"
#include "ui_dataimportform.h"

DataImportForm::DataImportForm(StringHash userInfo, QWidget *parent) :
    userInfo(userInfo),
    QWidget(parent),
    ui(new Ui::DataImportForm)
{
    ui->setupUi(this);
}

DataImportForm::~DataImportForm()
{
    delete ui;
}

void DataImportForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
