#include "customersearchform.h"
#include "ui_customersearchform.h"
#include "restmanager.h"
#include "callstatus.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QPointer>

CustomerSearchForm::CustomerSearchForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CustomerSearchForm)
{
    ui->setupUi(this);

    searchParams = StringHash();

    setupCustomerSearch();
    setupCustomerTable();
    toggleSearchProgress(false);

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString,Http::Method,QByteArray)),
            SLOT(onRestReplyReceived(int,QString,QString,Http::Method,QByteArray)));
    connect(searchModel, SIGNAL(columnSorting(QString,QString)), SLOT(onModelColumnSorting(QString,QString)));
    connect(searchModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), SLOT(onDataChanged(QModelIndex,QModelIndex)));
}

CustomerSearchForm::~CustomerSearchForm()
{
    delete ui;
}

void CustomerSearchForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void CustomerSearchForm::setupCustomerSearch()
{
    ui->sex->addItem("");
    ui->sex->addItem(QIcon(":/label/male"), "Laki-laki", "MALE");
    ui->sex->addItem(QIcon(":/label/female"), "Perempuan", "FEMALE");

    ui->productStatus->addItem("");
    ui->productStatus->addItem("Approved");
    ui->productStatus->addItem("Reconfirm");
    ui->productStatus->addItem("Reject");
    ui->productStatus->addItem("Back Date");
    ui->productStatus->addItem("Cancel");
    ui->productStatus->addItem("Confirmed");

    CallStatus::getInstance();

    QPointer<RestManager> restManager = RestManager::getInstance();

    restManager->sendGet("campaign");
    restManager->sendGet("call_activity/disposition");
}

void CustomerSearchForm::setupCustomerTable()
{
    searchModel = new SearchModel("/provider");

    searchSortModel = new SearchSortModel;
    searchSortModel->setSourceModel(searchModel);

    ui->customerTable->setModel(searchModel);
    ui->customerTable->hideColumn(0);
}

void CustomerSearchForm::setupCampaign(QByteArray data)
{
    QVariantList campaigns = QJsonDocument::fromJson(data).array().toVariantList();

    ui->campaign->addItem("", 0);

    foreach (QVariant campaign, campaigns) {
        QVariantMap campaignFields = campaign.toMap();

        ui->campaign->addItem(campaignFields["name"].toString(), campaignFields["id"]);
    }
}

void CustomerSearchForm::setupCallStatus(QByteArray data)
{
    QVariantList callStatuses = QJsonDocument::fromJson(data).array().toVariantList();

    ui->callStatus->clear();
    ui->callStatus->addItem("", 0);

    foreach (QVariant callStatus, callStatuses) {
        QVariantMap callStatusMap = callStatus.toMap();

        ui->callStatus->addItem(callStatusMap["name"].toString(), callStatusMap["status"]);
    }
}

void CustomerSearchForm::toggleSearchProgress(bool toggle)
{
    ui->searchProgress->setMaximum(toggle ? 0 : 1);
    ui->searchProgress->setValue(toggle ? -1 : 1);
    ui->searchProgress->setVisible(toggle);
}

void CustomerSearchForm::resetNavigationState()
{
    int pageNumber = ui->pageNumber->value(),
        pageCount = QVariant(ui->pageCount->text()).toInt();
    bool first = true, previous = true, next = true, last = true;

    if (pageNumber <= 1) {
        first = false;
        previous = false;
    }

    if (pageNumber >= pageCount) {
        next = false;
        last = false;
    }

    ui->firstPage->setEnabled(first);
    ui->previousPage->setEnabled(previous);
    ui->nextPage->setEnabled(next);
    ui->lastPage->setEnabled(last);
}

void CustomerSearchForm::retrieveCustomers()
{
    QString pageNumberString;
    int pageNumber = ui->pageNumber->value();

    if (searchPageNumber == pageNumber) return;

    searchPageNumber = pageNumber;

    if (pageNumber == 0) {
        pageNumberString = "1";
    } else {
        pageNumberString = QVariant(pageNumber).toString();
    }

    RestManager::getInstance()->sendGet(QString("customer/page/").append(pageNumberString), searchParams);

    toggleSearchProgress();
}

void CustomerSearchForm::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if (path == "campaign") {
            setupCampaign(data);
        } else if (path == "customer/page") {
            QVariantMap pages = QJsonDocument::fromJson(data).object().toVariantMap();

            int pageCount = pages["last"].toInt(),
                pageNumber = pageCount < 1 ? 0 : 1;

            ui->pageNumber->setMinimum(pageNumber);
            ui->pageNumber->setMaximum(pageCount);
            ui->pageNumber->setValue(pageNumber);
            ui->pageCount->setText(pages["last"].toString());

            resetNavigationState();
        } else if (path.startsWith("customer/page/") || path.startsWith("customer/detail/")) {
            ui->customerTable->resizeColumnsToContents();

            toggleSearchProgress(false);
        } else if (path == "call_activity/disposition") {
            setupCallStatus(data);
        }
    }
}

void CustomerSearchForm::onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    ui->customerTable->setVisible(false);
    ui->customerTable->resizeColumnsToContents();
    ui->customerTable->resizeRowsToContents();
    ui->customerTable->setVisible(true);
}

void CustomerSearchForm::onModelColumnSorting(QString order, QString sort)
{
    searchParams["order"] = order;
    searchParams["sort"] = sort;

//    toggleSearchFields();

    RestManager::getInstance()->sendGet("/provider", searchParams);
}

void CustomerSearchForm::on_search_clicked()
{
    QString name = ui->name->text(),
            sex = ui->sex->itemData(ui->sex->currentIndex()).toString(),
            dob = ui->dateOfBirth->date().toString("dd/MM/yyyy") == "01/01/2000" ?
                "" : ui->dateOfBirth->date().toString("yyyy-MM-dd"),
            address = ui->address->text(),
            city = ui->city->text(),
            postalCode = ui->postalCode->text(),
            phone = ui->phoneNumber->text(),
            code = ui->customerCode->text(),
            campaign = ui->campaign->itemData(ui->campaign->currentIndex()).toString(),
            callStatusId = ui->callStatus->itemData(ui->callStatus->currentIndex()).toString(),
            callDateFrom = ui->callDateFrom->date().toString("dd/MM/yyyy") == "01/01/2000" ?
                "" : ui->callDateFrom->date().toString("yyyy-MM-dd"),
            callDateTo = ui->callDateTo->date().toString("dd/MM/yyyy") == "01/01/2000" ?
                "" : ui->callDateTo->date().toString("yyyy-MM-dd"),
            productStatus = ui->productStatus->currentText();

    searchParams.clear();

    if (!name.isEmpty()) {
        searchParams.insert("name", name);
    }
    if (!sex.isEmpty()) {
        searchParams.insert("sex", sex);
    }
    if (!dob.isEmpty()) {
        searchParams.insert("dob", dob);
    }
    if (!address.isEmpty()) {
        searchParams.insert("address", address);
    }
    if (!city.isEmpty()) {
        searchParams.insert("city", city);
    }
    if (!postalCode.isEmpty()) {
        searchParams.insert("postal_code", postalCode);
    }
    if (!phone.isEmpty()) {
        searchParams.insert("phone", phone);
    }
    if (!code.isEmpty()) {
        searchParams.insert("code", code);
    }
    if (campaign != "0") {
        searchParams.insert("campaign_id", campaign);
    }
    if (callStatusId != "0") {
        searchParams.insert("call_disposition_id", callStatusId);
    }
    if (!callDateFrom.isEmpty()) {
        searchParams.insert("call_date_from", callDateFrom);
    }
    if (!callDateTo.isEmpty()) {
        searchParams.insert("call_date_to", callDateTo);
    }
    if (!productStatus.isEmpty()) {
        searchParams.insert("product_status", productStatus);
    }

    QPointer<RestManager> restManager = RestManager::getInstance();

    restManager->sendGet("customer/page", searchParams);
    restManager->sendGet("customer/page/1", searchParams);

    toggleSearchProgress();
}

void CustomerSearchForm::on_clear_clicked()
{
    ui->name->clear();
    ui->dateOfBirth->setDate(QDate(2000, 1, 1));
    ui->sex->setCurrentIndex(0);
    ui->address->clear();
    ui->city->clear();
    ui->postalCode->clear();
    ui->phoneNumber->clear();
    ui->customerCode->clear();
    ui->campaign->setCurrentIndex(0);
    ui->callStatus->setCurrentIndex(0);
    ui->callDateFrom->setDate(QDate(2000, 1, 1));
    ui->callDateTo->setDate(QDate(2000, 1, 1));
    ui->productStatus->setCurrentIndex(0);
}

void CustomerSearchForm::on_firstPage_clicked()
{
    ui->pageNumber->setValue(ui->pageNumber->minimum());

    retrieveCustomers();
    resetNavigationState();
}

void CustomerSearchForm::on_previousPage_clicked()
{
    ui->pageNumber->stepDown();

    retrieveCustomers();
    resetNavigationState();
}

void CustomerSearchForm::on_nextPage_clicked()
{
    ui->pageNumber->stepUp();

    retrieveCustomers();
    resetNavigationState();
}

void CustomerSearchForm::on_lastPage_clicked()
{
    ui->pageNumber->setValue(ui->pageNumber->maximum());

    retrieveCustomers();
    resetNavigationState();
}

void CustomerSearchForm::on_pageNumber_editingFinished()
{
    retrieveCustomers();
    resetNavigationState();
}

void CustomerSearchForm::on_customerTable_doubleClicked(const QModelIndex &index)
{
    QString customerId = searchModel->getColumnValue(index),
            customerCode = searchModel->getColumnValue(index, 1);

    if (!customerId.isEmpty()) {
//        searchModel->getCustomerDetail(index);

        emit customerTableClicked(customerId, customerCode);

        toggleSearchProgress();
    }
}
