#include "qacallscoringform.h"
#include "ui_qacallscoringform.h"

#include <QDateTime>
#include <QPointer>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QButtonGroup>
#include <QRadioButton>

QACallScoringForm::QACallScoringForm(QString customerId, QWidget *parent) :
    customerId(customerId),
    QWidget(parent),
    ui(new Ui::QACallScoringForm)
{
    ui->setupUi(this);

    requestCode = QDateTime::currentDateTime().toString("yyyyMMddHHmmsszzz");

    setupScoreSheet();

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestreplyReceived(int,QString,QString, Http::Method,QByteArray)));

    RestManager::getInstance()->sendGet(QString("call_scoring/group/").append(requestCode));
}

QACallScoringForm::~QACallScoringForm()
{
    delete ui;
}

void QACallScoringForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void QACallScoringForm::setupScoreSheet()
{
    ui->product->addItem("");
    ui->product->addItem("Life Protector Plus");

    ui->scoreSheet->setHorizontalHeaderLabels(QStringList() << "Category" << "Criteria" << "Good" << "Average" << "Poor" << "Id");

    ui->scoreSheet->setColumnWidth(0, 120);
    ui->scoreSheet->setColumnWidth(1, 300);

    ui->scoreSheet->hideColumn(5);
}

void QACallScoringForm::lockCallScoringForm()
{
    ui->product->setDisabled(true);
    ui->sellingDate->setDisabled(true);
    ui->duration->setDisabled(true);
    ui->scoreSheet->setDisabled(true);
    ui->saveScore->setDisabled(true);
}

void QACallScoringForm::populateGroup(QByteArray data)
{
    QVariantList groups = QJsonDocument::fromJson(data).array().toVariantList();

    foreach (QVariant group, groups) {
        QVariantMap groupFields = group.toMap();

        scoringGroup.insert(groupFields["id"].toInt(), groupFields["group"].toString());
    }
}

void QACallScoringForm::populateCriteria(QByteArray data)
{
    QVariantList criteria = QJsonDocument::fromJson(data).array().toVariantList();

    foreach (QVariant criterion, criteria) {
        QVariantMap criteriaFields = criterion.toMap();
        Criteria criteria = {
            criteriaFields["criteria"].toString(),
            criteriaFields["good"].toInt(),
            criteriaFields["average"].toInt(),
            criteriaFields["poor"].toInt()
        };

        scoringCriteria.insert(criteriaFields["id"].toInt(), criteria);
        criteriaGroup.insert(criteriaFields["id"].toInt(), criteriaFields["group_id"].toInt());
    }
}

void QACallScoringForm::populateScoreSheet()
{
    QMapIterator<int, Criteria> i(scoringCriteria);
    int row = 0;

    while (i.hasNext()) {
        i.next();

        ui->scoreSheet->setRowCount(row + 1);
        ui->scoreSheet->setItem(row, 0, new QTableWidgetItem(scoringGroup.value(criteriaGroup.value(i.key()))));
        ui->scoreSheet->setItem(row, 1, new QTableWidgetItem(i.value().criteria));
        ui->scoreSheet->setItem(row, 5, new QTableWidgetItem(QString::number(i.key())));

        QButtonGroup *group = new QButtonGroup;
        QRadioButton *good = new QRadioButton(QString::number(i.value().good)),
                *average = new QRadioButton(QString::number(i.value().average)),
                *poor = new QRadioButton(QString::number(i.value().poor));

        group->addButton(good);
        group->addButton(average);
        group->addButton(poor);

        ui->scoreSheet->setCellWidget(row, 2, good);
        ui->scoreSheet->setCellWidget(row, 3, average);
        ui->scoreSheet->setCellWidget(row, 4, poor);

        row++;
    }
}

void QACallScoringForm::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if (path == QString("call_scoring/group/").append(requestCode)) {
            populateGroup(data);

            RestManager::getInstance()->sendGet(QString("call_scoring/criteria/").append(requestCode));
        } else if (path == QString("call_scoring/criteria/").append(requestCode)) {
            populateCriteria(data);
            populateScoreSheet();

            RestManager::getInstance()->sendGet(QString("call_scoring/check/").append(customerId));
        } else if (path == QString("call_scoring/score/").append(customerId)) {
            RestManager::message(this, data, "Call Scoring");

            lockCallScoringForm();
        } else if (path == QString("call_scoring/check/").append(customerId)) {
            QVariantMap callScoring = QJsonDocument::fromJson(data).object().toVariantMap();

            qDebug(callScoring["id"].toString().toLatin1().data());

            if (callScoring["id"].toInt() > 0) lockCallScoringForm();
        }
    }
}

void QACallScoringForm::on_saveScore_clicked()
{
    StringHash params = StringHash();

    params["product"] = ui->product->currentText();
    params["selling_date"] = ui->sellingDate->date().toString("yyyy-MM-dd");
    params["duration"] = ui->duration->text();

    for (int row = 0; row < ui->scoreSheet->rowCount(); row++) {
        QString id = ui->scoreSheet->item(row, 5)->text();

        params[QString("good[%1]").arg(id)] = ((QRadioButton *) ui->scoreSheet->cellWidget(row, 2))->isChecked() ? "1" : "0";
        params[QString("average[%1]").arg(id)] = ((QRadioButton *) ui->scoreSheet->cellWidget(row, 3))->isChecked() ? "1" : "0";
        params[QString("poor[%1]").arg(id)] = ((QRadioButton *) ui->scoreSheet->cellWidget(row, 4))->isChecked() ? "1" : "0";
    }

    RestManager::getInstance()->sendPut(QString("call_scoring/score/").append(customerId), params);
}
