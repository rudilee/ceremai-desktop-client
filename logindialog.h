#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include <QAbstractButton>
#include <QSettings>

#include "restmanager.h"

namespace Ui {
    class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

    void on_login_clicked();
    void on_cancel_clicked();

public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::LoginDialog *ui;

    QSettings settings;

    void setupServersList();
    void clearFields();
};

#endif // LOGINDIALOG_H
