#ifndef QATABWIDGET_H
#define QATABWIDGET_H

#include <QWidget>
#include <QMap>
#include <QPointer>

#include "types.h"
#include "searchdetailtab.h"
#include "qasearchmodel.h"
#include "qasearchform.h"

class QATabWidget : public QWidget
{
    Q_OBJECT

private slots:
    void onPolisTableClicked(QString customerId, QString customerCode);

public:
    explicit QATabWidget(StringHash userInfo, QWidget *parent = 0);

private:
    StringHash userInfo;
    QPointer<SearchDetailTab> tabWidget;
    QPointer<QASearchModel> searchModel;
    QPointer<QASearchForm> searchForm;

    void setupSearchForm();
};

#endif // QATABWIDGET_H
