#ifndef TYPES_H
#define TYPES_H

#include <QHash>

typedef QHash<QString, QString> StringHash;
typedef QHashIterator<QString, QString> StringHashIterator;

typedef QHash<int, QString> IntHash;
typedef QHashIterator<int, QString> IntHashIterator;

#endif // TYPES_H
