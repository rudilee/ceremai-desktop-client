#ifndef RESTMANAGER_H
#define RESTMANAGER_H

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QStringList>

#include "types.h"

namespace Http {
    typedef enum {
        Unknown,
        Head,
        Get,
        Put,
        Post,
        Delete,
        Custom
    } Method;
}

class RestManager : public QNetworkAccessManager
{
    Q_OBJECT
    Q_DISABLE_COPY(RestManager)

signals:
    void replyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

private slots:
    void onRequestFinished(QNetworkReply *reply);

public:
    static RestManager *getInstance();

    void setBaseUrl(QString baseUrl);

    void sendGet(QString path, StringHash params = StringHash(), StringHash headers = StringHash());
    void sendPost(QString path, StringHash params = StringHash(), bool isPut = false);
    void sendPut(QString path, StringHash params = StringHash());
    void sendDelete(QString url);

    static void message(QWidget *parent, QString title, QByteArray data, bool isWarning = false);

private:
    static RestManager *instance;

    QString baseUrl;
    QString appPath;
    QByteArray userAgent;
    QStringList methodList;

    explicit RestManager(QObject *parent = 0);

    QNetworkRequest setupRequest();
};

#endif // RESTMANAGER_H
