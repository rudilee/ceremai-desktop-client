#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPointer>
#include <QLabel>
#include <QDateTimeEdit>
#include <QBasicTimer>

#include "logindialog.h"
#include "dashboardform.h"
#include "customertabwidget.h"
#include "qatabwidget.h"
#include "dataimportform.h"
#include "dataexportform.h"
#include "distributeform.h"
#include "reportform.h"
#include "manageuserform.h"
#include "changepasswordform.h"
#include "userinfoform.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    Q_DISABLE_COPY(MainWindow)

private slots:
    void onLoginDialogAccepted();
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);
    void onCtiFeedbackReceived(QString feedback, bool error);

    void on_actionLogout_triggered(bool checked = false);
    void on_actionDashboard_triggered(bool checked = false);
    void on_actionSearchCustomers_triggered(bool checked = false);
    void on_actionQualityAssurance_triggered(bool checked = false);
    void on_actionDataImport_triggered(bool checked = false);
    void on_actionDataExport_triggered(bool checked = false);
    void on_actionDistributeCustomer_triggered(bool checked = false);
    void on_actionReport_triggered(bool checked = false);
    void on_actionChangePassword_triggered(bool checked = false);
    void on_actionManageUser_triggered(bool checked = false);

public:
    static MainWindow *getInstance();

    ~MainWindow();

    QDateTime getServerTime();

protected:
    void changeEvent(QEvent *e);
    void closeEvent(QCloseEvent *event);
    void timerEvent(QTimerEvent *event);

private:
    Ui::MainWindow *ui;

    static MainWindow *instance;

    StringHash userInfo;
    QMap<QString, QList<bool> > privileges;

    QDateTimeEdit *serverTime;
    QBasicTimer timer;

    QPointer<LoginDialog> loginDialog;
    QPointer<DashboardForm> dashboardForm;
    QPointer<CustomerTabWidget> customerTabWidget;
    QPointer<QATabWidget> qaTabWidget;
    QPointer<DataImportForm> dataImportForm;
    QPointer<DataExportForm> dataExportForm;
    QPointer<DistributeForm> distributeForm;
    QPointer<ReportForm> reportForm;
    QPointer<ManageUserForm> manageUserForm;
    QPointer<ChangePasswordForm> changePasswordForm;
    QPointer<UserInfoForm> userInfoForm;

    bool isSessionDone;

    explicit MainWindow(QWidget *parent = 0);

    QString getAppFullname();
    void setupPrivileges();
    void setupUserInfoForm();
    void setupToolbarItem();
    void setupSalesScriptForm();
    bool customerFormCreated();
    bool alertActivityChanged();
    void setModuleTitle(QString title);
};

#endif // MAINWINDOW_H
