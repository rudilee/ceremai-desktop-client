#ifndef MANAGEUSERFORM_H
#define MANAGEUSERFORM_H

#include <QWidget>
#include <QMap>
#include <QModelIndex>

#include "types.h"
#include "restmanager.h"

namespace Ui {
    class ManageUserForm;
}

class ManageUserForm : public QWidget
{
    Q_OBJECT

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

    void on_addUser_clicked();
    void on_editUser_clicked();
    void on_deleteUser_clicked();
    void on_save_clicked();
    void on_cancel_clicked();
    void on_usersTable_clicked(QModelIndex index);

public:
    explicit ManageUserForm(StringHash userInfo, QWidget *parent = 0);
    ~ManageUserForm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::ManageUserForm *ui;

    StringHash userInfo;
    QMap<int, QStringList> userRole;
    QString currentUserId;

    enum State {
        Idle, Selecting, Adding, Editing
    } state;

    void setupUsersTable();
    void setupUserRole(QByteArray data);
    void setupSuperior(QByteArray data);
    void populateUsersTable(QByteArray data);
    void fillUserFields(QByteArray data);
    void changeState(State state);
    void clearUserFields();
    void toggleRequiredField(bool update = false);
    void refreshUserData();
};

#endif // MANAGEUSERFORM_H
