#ifndef QASEARCHFORM_H
#define QASEARCHFORM_H

#include <QWidget>
#include <QMap>
#include <QPointer>

#include "types.h"
#include "restmanager.h"
#include "qasearchmodel.h"

namespace Ui {
    class QASearchForm;
}

class QASearchForm : public QWidget
{
    Q_OBJECT

signals:
    void polisTableClicked(QString customerId, QString customerCode);

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

    void on_searchButton_clicked();
    void on_clearButton_clicked();
    void on_firstButton_clicked();
    void on_previousButton_clicked();
    void on_nextButton_clicked();
    void on_lastButton_clicked();
    void on_currentPage_editingFinished();
    void on_polisTable_doubleClicked(const QModelIndex &index);

public:
    explicit QASearchForm(QWidget *parent = 0);
    ~QASearchForm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::QASearchForm *ui;

    StringHash searchParams;
    QPointer<QASearchModel> searchModel;
    int searchCurrentPage;

    void setupPolisSearch();
    void setupPolisTable();
    void setupCampaignSelect(QByteArray data);
    void setupCustomerDetail(QByteArray data);
    void resetNavigationState();
    void toggleSearchProgress(bool toggle = true);
    void retrieveCustomers();
};

#endif // QASEARCHFORM_H
