#ifndef QACALLSCORINGFORM_H
#define QACALLSCORINGFORM_H

#include <QWidget>
#include <QMap>

#include "restmanager.h"

namespace Ui {
class QACallScoringForm;
}

class QACallScoringForm : public QWidget
{
    Q_OBJECT

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

    void on_saveScore_clicked();

public:
    explicit QACallScoringForm(QString customerId, QWidget *parent = 0);
    ~QACallScoringForm();
    
protected:
    void changeEvent(QEvent *e);
    
private:
    Ui::QACallScoringForm *ui;

    struct Criteria {
        QString criteria;
        int good;
        int average;
        int poor;
    };

    QString customerId;
    QString requestCode;
    QMap<int, QString> scoringGroup;
    QMap<int, Criteria> scoringCriteria;
    QMap<int, int> criteriaGroup;

    void setupScoreSheet();
    void lockCallScoringForm();
    void populateGroup(QByteArray data);
    void populateCriteria(QByteArray data);
    void populateScoreSheet();
};

#endif // QACALLSCORINGFORM_H
