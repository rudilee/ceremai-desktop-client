#include "customerdetailform.h"
#include "ui_customerdetailform.h"
#include "cticlient.h"
#include "mainwindow.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>
#include <QDebug>

CustomerDetailForm::CustomerDetailForm(StringHash userInfo, QString customerId, QWidget *parent) :
    userInfo(userInfo),
    customerId(customerId),
    QWidget(parent),
    ui(new Ui::CustomerDetailForm)
{
    ui->setupUi(this);

    phoneNumbers = StringHash();
    activityChanged = false;

    setupStartCallMenu();
    setupCallActivity();
    setupProductTabs();

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestReplyReceived(int,QString,QString, Http::Method,QByteArray)));
    connect(startCallMenu, SIGNAL(triggered(QAction*)), SLOT(onStartCallMenuTriggered(QAction*)));
    connect(ui->showMemo, SIGNAL(clicked()), &memoDialog, SLOT(show()));
}

CustomerDetailForm::~CustomerDetailForm()
{
    delete ui;
}

void CustomerDetailForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void CustomerDetailForm::closeEvent(QCloseEvent *event)
{
    if (activityChanged) {
        event->ignore();

        ui->detailTabs->setCurrentIndex(0);

        QMessageBox::warning(this, "Call Activity Changed", "Silahkan simpan dulu hasil kerja anda");
    }
}

void CustomerDetailForm::setupCustomerDetail(QByteArray data)
{
    QVariantMap customer = QJsonDocument::fromJson(data).object().toVariantMap();

    ui->nameField->setText(customer["name"].toString());
    ui->dobField->setText(QDate::fromString(customer["date_of_birth"].toString(), "yyyy-MM-dd").toString("d MMMM yyyy"));
    ui->ageField->setText(customer["age"].toString().append(" Tahun"));

    QString sex = customer["sex"].toString();
    ui->sexField->setText(sex.isEmpty() ? "" : (sex == "MALE" ? "Laki-laki" : "Perempuan"));
    ui->sexField->setStyleSheet(QString("background-color: rgba(170, 255, 127, 50);"
                                        "background-image: url(:/label/%1);"
                                        "background-repeat: no;"
                                        "background-position: left center;"
                                        "padding-left: 16;").arg(sex.isEmpty() ? "no_gender" : (sex == "MALE" ? "male" : "female")));

    ui->homeStreet1->setText(customer["home_street_1"].toString());
    ui->homeStreet2->setText(customer["home_street_2"].toString());
    ui->homeStreet3->setText(customer["home_street_3"].toString());
    ui->homeCity->setText(customer["home_city"].toString());
    ui->homePostal->setText(customer["home_postal_code"].toString());
    ui->officeEmployer->setText(customer["office_employer"].toString());
    ui->officeStreet1->setText(customer["office_street_1"].toString());
    ui->officeStreet2->setText(customer["office_street_2"].toString());
    ui->officeStreet3->setText(customer["office_street_3"].toString());
    ui->officeCity->setText(customer["office_city"].toString());
    ui->officePostal->setText(customer["office_postal_code"].toString());
    ui->additionalStreet1->setText(customer["additional_street_1"].toString());
    ui->additionalStreet2->setText(customer["additional_street_2"].toString());
    ui->additionalStreet3->setText(customer["additional_street_3"].toString());
    ui->additionalCity->setText(customer["additional_city"].toString());
    ui->additionalPostalCode->setText(customer["additional_postal_code"].toString());

    ui->homePhone->setText(customer["home_phone"].toString());
    ui->homePhone2->setText(customer["home_phone_2"].toString());
    ui->homePhone3->setText(customer["home_phone_3"].toString());
    ui->officePhone->setText(customer["office_phone"].toString());
    ui->officePhone2->setText(customer["office_phone_2"].toString());
    ui->officePhone3->setText(customer["office_phone_3"].toString());
    ui->mobilePhone->setText(customer["mobile_phone"].toString());
    ui->mobilePhone2->setText(customer["mobile_phone_2"].toString());
    ui->mobilePhone3->setText(customer["mobile_phone_3"].toString());

    memoDialog.setWindowTitle(customer["code"].toString());
    memoDialog.setMemoContent(customer["memo"].toString());

    customerInfo.clear();

    QMapIterator<QString, QVariant> iterator(customer);
    while (iterator.hasNext()) {
        iterator.next();

        customerInfo.insert(iterator.key(), iterator.value().toString());
    }

    updatePhoneNumbers();

    phoneNumbers["Home Phone"] = ui->homePhone->text();
    phoneNumbers["Office Phone"] = ui->officePhone->text();
    phoneNumbers["Mobile Phone"] = ui->mobilePhone->text();

    clearCallActivity();
}

void CustomerDetailForm::setupStartCallMenu()
{
    startCallMenu = new QMenu("Call");
    startCallMenu->addAction(QIcon(":/label/icons/telephone.png"), "Home Phone")->setIconVisibleInMenu(true);
    startCallMenu->addAction(QIcon(":/label/icons/telephone.png"), "Office Phone")->setIconVisibleInMenu(true);
    startCallMenu->addAction(QIcon(":/label/icons/phone.png"), "Mobile Phone")->setIconVisibleInMenu(true);
    startCallMenu->addSeparator();
    startCallMenu->addAction(QIcon(":/label/icons/telephone.png"), "Home Phone 2")->setIconVisibleInMenu(true);
    startCallMenu->addAction(QIcon(":/label/icons/telephone.png"), "Home Phone 3")->setIconVisibleInMenu(true);
    startCallMenu->addAction(QIcon(":/label/icons/telephone.png"), "Office Phone 2")->setIconVisibleInMenu(true);
    startCallMenu->addAction(QIcon(":/label/icons/telephone.png"), "Office Phone 3")->setIconVisibleInMenu(true);
    startCallMenu->addAction(QIcon(":/label/icons/phone.png"), "Mobile Phone 2")->setIconVisibleInMenu(true);
    startCallMenu->addAction(QIcon(":/label/icons/phone.png"), "Mobile Phone 3")->setIconVisibleInMenu(true);

    ui->startCall->setMenu(startCallMenu);
}

void CustomerDetailForm::setupCallActivity()
{
    QStringList labels;
    labels << "Called Date" << "Agent" << "Fullname" << "Call Disposition" << "Called Number" << "Follow Up" << "Remarks";

    ui->callHistory->setHorizontalHeaderLabels(labels);
    ui->callHistory->setColumnWidth(0, 120);
    ui->callHistory->setColumnWidth(1, 100);
    ui->callHistory->setColumnWidth(2, 120);
    ui->callHistory->setColumnWidth(4, 150);
    ui->callHistory->setColumnWidth(5, 120);
    ui->followUpDate->setDate(QDate::currentDate());

    RestManager::getInstance()->sendGet("call_activity/disposition");
}

void CustomerDetailForm::setupCallStatus(QByteArray data)
{
    QVariantList callStatuses = QJsonDocument::fromJson(data).array().toVariantList();

    ui->callStatus->clear();
    ui->callStatus->addItem("", 0);

    foreach (QVariant callStatus, callStatuses) {
        QVariantMap callStatusMap = callStatus.toMap();

        ui->callStatus->addItem(callStatusMap["disposition"].toString(), callStatusMap["id"]);
    }
}

void CustomerDetailForm::setupProductTabs()
{
}

void CustomerDetailForm::populateCallActivity(QByteArray data)
{
    QVariantList histories = QJsonDocument::fromJson(data).array().toVariantList();

    ui->callHistory->clearContents();
    ui->callHistory->setRowCount(0);

    int row = 0;
    foreach (QVariant history, histories) {
        QVariantMap historyFields = history.toMap();

        ui->callHistory->setRowCount(row + 1);

        ui->callHistory->setItem(row, 0, new QTableWidgetItem(QDateTime::fromString(historyFields["called"].toString(), "yyyy-MM-dd hh:mm:ss").toString("dd/MM/yyyy H:mm:ss")));
        ui->callHistory->setItem(row, 1, new QTableWidgetItem(historyFields["username"].toString()));
        ui->callHistory->setItem(row, 2, new QTableWidgetItem(QString("%1 %2").arg(historyFields["first_name"].toString(), historyFields["last_name"].toString())));
        ui->callHistory->setItem(row, 3, new QTableWidgetItem(historyFields["call_disposition"].toString()));
        ui->callHistory->setItem(row, 4, new QTableWidgetItem(historyFields["called_number"].toString()));
        ui->callHistory->setItem(row, 5, new QTableWidgetItem(QDateTime::fromString(historyFields["follow_up"].toString(), "yyyy-MM-dd hh:mm:ss").toString("dd/MM/yyyy H:mm:ss")));
        ui->callHistory->setItem(row, 6, new QTableWidgetItem(historyFields["remarks"].toString()));

        row++;
    }

    ui->callHistory->resizeColumnsToContents();
}

void CustomerDetailForm::updatePhoneNumbers()
{
    phoneNumbers["Home Phone 2"] = ui->homePhone2->text();
    phoneNumbers["Home Phone 3"] = ui->homePhone3->text();
    phoneNumbers["Office Phone 2"] = ui->officePhone2->text();
    phoneNumbers["Office Phone 3"] = ui->officePhone3->text();
    phoneNumbers["Mobile Phone 2"] = ui->mobilePhone2->text();
    phoneNumbers["Mobile Phone 3"] = ui->mobilePhone3->text();
}

void CustomerDetailForm::clearCallActivity()
{
    calledNumber.clear();

    ui->calledNumber->clear();
    ui->calledDate->clear();
    ui->callStatus->setCurrentIndex(0);
    ui->followUpDate->setDateTime(QDateTime(QDate::currentDate(), QTime(0, 0)));
    ui->isFollowUp->setChecked(false);
    ui->callRemarks->clear();
}

void CustomerDetailForm::showActivityForm()
{
    ui->detailTabs->setCurrentIndex(0);
}

void CustomerDetailForm::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if (path == QString("customer/detail/").append(customerId)) {
            setupCustomerDetail(data);
        } else if (path == QString("call_activity/history/").append(customerId)) {
            populateCallActivity(data);
        } else if (path == QString("customer/phone/").append(customerId)) {
            RestManager::message(this, data, "Phone Update");

            updatePhoneNumbers();
        } else if (path == QString("customer/address/").append(customerId)) {
            RestManager::message(this, data, "Address Update");
        } else if (path == "call_activity/disposition") {
            setupCallStatus(data);
        } else if (path == QString("call_activity/activity/").append(customerId)) {
            activityChanged = false;

            clearCallActivity();

            RestManager::message(this, data, "Call Activity Save");
            RestManager::getInstance()->sendGet(QString("call_activity/history/").append(customerInfo["id"]));
        }
    }
}

void CustomerDetailForm::onStartCallMenuTriggered(QAction *action)
{
    calledNumber = phoneNumbers[action->text()];

    if (!calledNumber.isEmpty()) {
        if (activityChanged) {
            QMessageBox::warning(this, "Call Activity Changed", "Silahkan simpan dulu hasil kerja anda");

            showActivityForm();
        } else if (CtiClient::getInstance()->callPhoneNumber(QString(calledNumber), customerInfo["code"], customerInfo["campaign"])) {
            activityChanged = true;

            ui->calledNumber->setText(calledNumber);
            ui->calledDate->setText(MainWindow::getInstance()->getServerTime().toString("dd/MM/yyyy hh:mm:ss"));
        }
    }
}

void CustomerDetailForm::on_saveActivity_clicked()
{
    if (!activityChanged) return;

    StringHash params = StringHash();
    QString followUp = ui->followUpDate->dateTime().toString("yyyy-MM-dd hh:mm:ss"),
            remarks = ui->callRemarks->toPlainText();

    params["called"] = QDateTime::fromString(ui->calledDate->text(), "dd/MM/yyyy hh:mm:ss").toString("yyyy-MM-dd hh:mm:ss");
    params["called_number"] = calledNumber;
    params["call_status_id"] = ui->callStatus->itemData(ui->callStatus->currentIndex()).toString();

    if (ui->isFollowUp->isChecked()) {
        params["follow_up"] = followUp;
    }

    if (!remarks.isEmpty()) {
        params["remarks"] = remarks;
    }

    qDebug() << "Params:" << params;

    RestManager::getInstance()->sendPost(QString("call_activity/activity/").append(customerInfo["id"]), params);
}

void CustomerDetailForm::on_updatePhone_clicked()
{
    if (customerInfo.contains("id")) {
        StringHash params = StringHash();

        params["home_phone_2"] = ui->homePhone2->text();
        params["home_phone_3"] = ui->homePhone3->text();
        params["office_phone_2"] = ui->officePhone2->text();
        params["office_phone_3"] = ui->officePhone3->text();
        params["mobile_phone_2"] = ui->mobilePhone2->text();
        params["mobile_phone_3"] = ui->mobilePhone3->text();

        RestManager::getInstance()->sendPut(QString("customer/phone/").append(customerInfo["id"]), params);
    }
}

void CustomerDetailForm::on_updateAddress_clicked()
{
    if (customerInfo.contains("id")) {
        StringHash params = StringHash();

        params["street_1"] = ui->additionalStreet1->text();
        params["street_2"] = ui->additionalStreet2->text();
        params["street_3"] = ui->additionalStreet3->text();
        params["city"] = ui->additionalCity->text();
        params["postal_code"] = ui->additionalPostalCode->text();

        RestManager::getInstance()->sendPut(QString("customer/address/").append(customerInfo["id"]), params);
    }
}

void CustomerDetailForm::on_stopCall_clicked()
{
    CtiClient::getInstance()->hangupPhone();
}

void CustomerDetailForm::on_followUpDate_dateTimeChanged(QDateTime datetime)
{
    ui->followUpDate->setSelectedSection(QDateTimeEdit::DaySection);
    ui->isFollowUp->setChecked(true);
}
