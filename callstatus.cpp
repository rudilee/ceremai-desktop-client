#include "callstatus.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QVariantList>

CallStatus *CallStatus::instance = NULL;

CallStatus::CallStatus(QObject *parent) :
    QObject(parent)
{
    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString,Http::Method,QByteArray)),
            SLOT(onRestReplyReceived(int,QString,QString,Http::Method,QByteArray)));
}

CallStatus *CallStatus::getInstance()
{
    if (instance == NULL) {
        instance = new CallStatus;
    }

    return instance;
}

void CallStatus::setupCallStatus(QByteArray data)
{
    QVariantList callStatusList = QJsonDocument::fromJson(data).array().toVariantList();

    callStatuses.clear();

    foreach (QVariant callStatus, callStatusList) {
        QVariantMap callStatusFields = callStatus.toMap();

        callStatuses.insert(callStatusFields["id"].toInt(), callStatusFields["status"].toString());
    }
}

void CallStatus::setupCallReason(QByteArray data)
{
    QVariantList callReasonList = QJsonDocument::fromJson(data).array().toVariantList();

    callReasons.clear();
    callReasonStatus.clear();

    foreach (QVariant callReason, callReasonList) {
        QVariantMap callReasonFields = callReason.toMap();

        callReasons.insert(callReasonFields["id"].toInt(), callReasonFields["reason"].toString());
        callReasonStatus.insertMulti(callReasonFields["call_status_id"].toInt(), callReasonFields["id"].toInt());
    }
}

QMap<int, QString> CallStatus::getCallStatuses()
{
    return callStatuses;
}

QMap<int, QString> CallStatus::getCallReasons(int callStatusId)
{
    QMap<int, QString> callReasonList;
    QMap<int, int>::const_iterator iterator = callReasonStatus.find(callStatusId);

    while (iterator != callReasonStatus.end() && iterator.key() == callStatusId) {
        int callReasonId = iterator.value();

        callReasonList.insert(callReasonId, callReasons[callReasonId]);

        ++iterator;
    }

    return callReasonList;
}

void CallStatus::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if (path == "call_activity/status") {
            setupCallStatus(data);
        } else if (path == "call_activity/reason") {
            setupCallReason(data);
        }
    }
}
