#include "customermemodialog.h"
#include "ui_customermemodialog.h"

CustomerMemoDialog::CustomerMemoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CustomerMemoDialog)
{
    ui->setupUi(this);

    setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::WindowStaysOnTopHint);
}

CustomerMemoDialog::~CustomerMemoDialog()
{
    delete ui;
}

void CustomerMemoDialog::setMemoContent(QString memoContent)
{
    ui->memoContent->setPlainText(memoContent.replace("|", "\n"));
}

void CustomerMemoDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
