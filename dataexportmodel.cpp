#include "dataexportmodel.h"

#include <QJsonDocument>
#include <QJsonArray>

DataExportModel::DataExportModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestReplyReceived(int,QString,QString, Http::Method,QByteArray)));
}

int DataExportModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return retrieveResult.count();
}

int DataExportModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return headers.count();
}

QVariant DataExportModel::data(const QModelIndex &index, int role) const
{
    if (index.isValid() && role == Qt::DisplayRole && index.row() < retrieveResult.count()) {
        QStringList retrieveItem = retrieveResult.at(index.row());

        return retrieveItem.at(index.column());
    }

    return QVariant();
}

QVariant DataExportModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            return QVariant(headers.at(section));
        } else if (orientation == Qt::Vertical) {
            return QVariant(++section);
        }
    }

    return QVariant();
}

Qt::ItemFlags DataExportModel::flags(const QModelIndex &index) const
{
    if (index.isValid()) {
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    }

    return 0;
}

void DataExportModel::setHeaders(QStringList headers)
{
    this->headers.clear();

    foreach (QString header, headers) {
        this->headers << header.toLower();
    }
}

void DataExportModel::setRetrievePath(QString retrievePath)
{
    this->retrievePath = retrievePath;
}

void DataExportModel::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if  (path == retrievePath) {
            QVariantList results = QJsonDocument::fromJson(data).array().toVariantList();

            // Hapus2in dulu row2 sebelumnya
            beginRemoveRows(QModelIndex(), 0, retrieveResult.size() - 1);
            endRemoveRows();

            retrieveResult.clear();

            beginInsertRows(QModelIndex(), 0, results.size() - 1);
            foreach (QVariant result, results) {
                QStringList resultRow;
                QVariantMap resultFields = result.toMap();
                QMapIterator<QString, QVariant> resultIterator(resultFields);

                // Siapin field2 kosong buat direplace nanti
                for (int i = 0; i < resultFields.size(); i++) {
                    resultRow << "";
                }

                while (resultIterator.hasNext()) {
                    resultIterator.next();

                    resultRow.replace(headers.indexOf(resultIterator.key()), resultIterator.value().toString());
                }

                retrieveResult.append(resultRow);
            }
            endInsertRows();

            emit dataChanged(createIndex(0, 0), createIndex(retrieveResult.count() - 1, headers.count() - 1));
        }
    }
}
