#ifndef CUSTOMERDETAILFORM_H
#define CUSTOMERDETAILFORM_H

#include <QWidget>
#include <QMap>
#include <QPointer>
#include <QMenu>
#include <QDateTime>

#include "types.h"
#include "restmanager.h"
#include "customermemodialog.h"

namespace Ui {
class CustomerDetailForm;
}

class CustomerDetailForm : public QWidget
{
    Q_OBJECT

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);
    void onStartCallMenuTriggered(QAction *action);

    void on_saveActivity_clicked();
    void on_updatePhone_clicked();
    void on_updateAddress_clicked();
    void on_stopCall_clicked();
    void on_followUpDate_dateTimeChanged(QDateTime datetime);

public:
    explicit CustomerDetailForm(StringHash userInfo, QString customerId, QWidget *parent = 0);
    ~CustomerDetailForm();

    void showActivityForm();
    
protected:
    void changeEvent(QEvent *e);
    void closeEvent(QCloseEvent *event);
    
private:
    Ui::CustomerDetailForm *ui;

    QString customerId;
    StringHash userInfo;
    StringHash customerInfo;
    StringHash phoneNumbers;
    QPointer<QMenu> startCallMenu;
    CustomerMemoDialog memoDialog;
    bool activityChanged;
    QString calledNumber;

    void setupStartCallMenu();
    void setupCallActivity();
    void setupCallStatus(QByteArray data);
    void setupProductTabs();
    void setupCustomerDetail(QByteArray data);
    void populateCallActivity(QByteArray data);
    void updatePhoneNumbers();
    void clearCallActivity();
};

#endif // CUSTOMERDETAILFORM_H
