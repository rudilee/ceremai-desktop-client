#include "qadetailform.h"
#include "ui_qadetailform.h"
#include "qacallscoringform.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDate>
#include <QMessageBox>
#include <QFile>
#include <QDir>
#include <QFileDialog>

QADetailForm::QADetailForm(QString customerId, QWidget *parent) :
    customerId(customerId),
    QWidget(parent),
    ui(new Ui::QADetailForm)
{
    ui->setupUi(this);
    ui->detailTab->setCurrentIndex(0);

    setupCallActivity();

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestreplyReceived(int,QString,QString, Http::Method,QByteArray)));

    getFormDetails();
}

QADetailForm::~QADetailForm()
{
    delete ui;
}

void QADetailForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void QADetailForm::setupCallActivity()
{
    QStringList labels;
    labels << "Last Called" << "Agent" << "Fullname" << "Call Status" << "Call Reason" << "Called Number" << "Follow Up"
           << "Remarks";

    ui->callHistory->setHorizontalHeaderLabels(labels);
}

void QADetailForm::getFormDetails()
{
    QPointer<RestManager> restManager = RestManager::getInstance();

    restManager->sendGet(QString("customer/detail/").append(customerId));
    restManager->sendGet(QString("call_activity/history/").append(customerId));
}

void QADetailForm::setupCustomerDetail(QByteArray data)
{
    QVariantMap customer = QJsonDocument::fromJson(data).object().toVariantMap();

    ui->nameField->setText(customer["name"].toString());
    ui->dobField->setText(QDate::fromString(customer["date_of_birth"].toString(), "yyyy-MM-dd").toString("d MMMM yyyy"));
    ui->ageField->setText(customer["age"].toString().append(" Tahun"));
    ui->sexField->setText(customer["sex"].toString());
    ui->homeStreet1->setText(customer["home_street_1"].toString());
    ui->homeStreet2->setText(customer["home_street_2"].toString());
    ui->homeStreet3->setText(customer["home_street_3"].toString());
    ui->homeCity->setText(customer["home_city"].toString());
    ui->homePostal->setText(customer["home_postal_code"].toString());
    ui->officeEmployer->setText(customer["office_employer"].toString());
    ui->officeStreet1->setText(customer["office_street_1"].toString());
    ui->officeStreet2->setText(customer["office_street_2"].toString());
    ui->officeStreet3->setText(customer["office_street_3"].toString());
    ui->officeCity->setText(customer["office_city"].toString());
    ui->officePostal->setText(customer["office_postal_code"].toString());
    ui->additionalStreet1->setText(customer["additional_street_1"].toString());
    ui->additionalStreet2->setText(customer["additional_street_2"].toString());
    ui->additionalStreet3->setText(customer["additional_street_3"].toString());
    ui->additionalCity->setText(customer["additional_city"].toString());
    ui->additionalPostalCode->setText(customer["additional_postal_code"].toString());

    ui->homePhone->setText(customer["home_phone"].toString());
    ui->homePhone2->setText(customer["home_phone_2"].toString());
    ui->homePhone3->setText(customer["home_phone_3"].toString());
    ui->officePhone->setText(customer["office_phone"].toString());
    ui->officePhone2->setText(customer["office_phone_2"].toString());
    ui->officePhone3->setText(customer["office_phone_3"].toString());
    ui->mobilePhone->setText(customer["mobile_phone"].toString());
    ui->mobilePhone2->setText(customer["mobile_phone_2"].toString());
    ui->mobilePhone3->setText(customer["mobile_phone_3"].toString());
}

void QADetailForm::setupCallActivityHistory(QByteArray data)
{
    QVariantList histories = QJsonDocument::fromJson(data).array().toVariantList();

    ui->callHistory->clearContents();
    ui->callHistory->setRowCount(0);

    int row = 0;
    foreach (QVariant history, histories) {
        QVariantMap historyFields = history.toMap();

        ui->callHistory->setRowCount(row + 1);

        ui->callHistory->setItem(row, 0, new QTableWidgetItem(historyFields["called"].toString()));
        ui->callHistory->setItem(row, 1, new QTableWidgetItem(historyFields["username"].toString()));
        ui->callHistory->setItem(row, 2, new QTableWidgetItem(historyFields["first_name"].toString()));
        ui->callHistory->setItem(row, 3, new QTableWidgetItem(historyFields["call_status"].toString()));
        ui->callHistory->setItem(row, 4, new QTableWidgetItem(historyFields["call_reason"].toString()));
        ui->callHistory->setItem(row, 5, new QTableWidgetItem(historyFields["called_number"].toString()));
        ui->callHistory->setItem(row, 6, new QTableWidgetItem(historyFields["follow_up"].toString()));
        ui->callHistory->setItem(row, 7, new QTableWidgetItem(historyFields["remarks"].toString()));

        row++;
    }

    ui->callHistory->resizeColumnsToContents();
}

void QADetailForm::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if (path == QString("customer/detail/").append(customerId)) {
            setupCustomerDetail(data);

            ui->detailTab->addTab(new QACallScoringForm(customerId), QIcon(":/button/call scoring"), "Call Scoring");

            RestManager::getInstance()->sendGet(QString("product_lpp/customer/").append(customerId));
        } else if (path == QString("call_activity/history/").append(customerId)) {
            setupCallActivityHistory(data);
        }
    }
}
