#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "restmanager.h"
#include "cticlient.h"

#include <QCloseEvent>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>
#include <QLineEdit>
#include <QDebug>

MainWindow *MainWindow::instance = NULL;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    loginDialog = new LoginDialog;
    loginDialog->show();

    on_actionDashboard_triggered();

    isSessionDone = false;

    setWindowState(Qt::WindowMaximized);

    ui->menuBar->hide();

    serverTime = new QDateTimeEdit;
    serverTime->setFocusPolicy(Qt::NoFocus);
    serverTime->setStyleSheet("background-color: rgba(0, 0, 0, 0);");
    serverTime->setFrame(false);
    serverTime->setReadOnly(true);
    serverTime->setButtonSymbols(QAbstractSpinBox::NoButtons);
    serverTime->setDisplayFormat("ddd dd MMM yyyy hh:mm:ss");
    ui->statusBar->addPermanentWidget(serverTime);

    setupPrivileges();
    setupUserInfoForm();
    setupSalesScriptForm();

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestReplyReceived(int,QString,QString, Http::Method,QByteArray)));
    connect(CtiClient::getInstance(), SIGNAL(feedbackReceived(QString,bool)), SLOT(onCtiFeedbackReceived(QString,bool)));
    connect(loginDialog, SIGNAL(accepted()), SLOT(onLoginDialogAccepted()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

MainWindow *MainWindow::getInstance()
{
    if (instance == NULL) instance = new MainWindow;

    return instance;
}

QDateTime MainWindow::getServerTime()
{
    return serverTime->dateTime();
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (alertActivityChanged()) {
        event->ignore();
    }
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer.timerId()) {
        serverTime->setTime(serverTime->time().addSecs(1));
    } else {
        QMainWindow::timerEvent(event);
    }
}

QString MainWindow::getAppFullname()
{
    return QCoreApplication::applicationName().append(" ").append(QCoreApplication::applicationVersion());
}

void MainWindow::setupPrivileges()
{
                                                            // sc       qa        di        de        dc        r           cp        mu
    privileges.insert("agent", QList<bool>()                << true <<  false <<  false <<  false <<  false <<  false <<    true <<   false);
    privileges.insert("team leader", QList<bool>()          << true <<  false <<  false <<  false <<  true <<   false <<    true <<   false);
    privileges.insert("supervisor", QList<bool>()           << true <<  false <<  true <<   false <<  true <<   true <<     true <<   false);
    privileges.insert("manager", QList<bool>()              << true <<  false <<  true <<   false <<  true <<   true <<     true <<   false);
    privileges.insert("quality assurance", QList<bool>()    << false << true <<   false <<  true <<   false <<  true <<     true <<   false);
    privileges.insert("administrator", QList<bool>()        << true <<  true <<   true <<   true <<   true <<   true <<     true <<   true);
}

void MainWindow::setupUserInfoForm()
{
    userInfoForm = new UserInfoForm;

    ui->mainToolBar->addWidget(userInfoForm);
}

void MainWindow::setupToolbarItem()
{
    QList<bool> privilege = privileges.value(userInfo["role"]);

    ui->actionSearchCustomers->setEnabled(privilege[0]);
    ui->actionQualityAssurance->setEnabled(privilege[1]);
    ui->actionDataImport->setEnabled(/*privilege[2]*/false);
    ui->actionDataExport->setEnabled(privilege[3]);
    ui->actionDistributeCustomer->setEnabled(privilege[4]);
    ui->actionReport->setEnabled(privilege[5]);
    ui->actionChangePassword->setEnabled(privilege[6]);
    ui->actionManageUser->setEnabled(privilege[7]);
}

void MainWindow::setupSalesScriptForm()
{
}

bool MainWindow::customerFormCreated()
{
     return (centralWidget() == customerTabWidget);
}

bool MainWindow::alertActivityChanged()
{
    if (customerFormCreated()) {
        return !customerTabWidget->close();
    }

    return false;
}

void MainWindow::setModuleTitle(QString title)
{
    setWindowTitle(QString("%1 - %2").arg(getAppFullname(), title));
}

void MainWindow::onLoginDialogAccepted()
{
    show();
}

void MainWindow::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if (path == "authentication/login") {
            QVariantMap userInfoMap = QJsonDocument::fromJson(data).object().toVariantMap();

            userInfo["user_id"] = userInfoMap["user_id"].toString();
            userInfo["username"] = userInfoMap["username"].toString();
            userInfo["fullname"] = userInfoMap["fullname"].toString();
            userInfo["role"] = userInfoMap["role"].toString();
            userInfo["role_name"] = userInfoMap["role_name"].toString();
            userInfo["role_id"] = userInfoMap["role_id"].toString();
            userInfo["datetime"] = userInfoMap["datetime"].toString();

            setupToolbarItem();

            userInfoForm->setUserInfo(userInfo["username"], userInfo["fullname"], userInfo["role"], userInfo["role_name"]);

            serverTime->setDateTime(QDateTime::fromString(userInfo["datetime"], "yyyy-MM-dd hh:mm:ss"));
            timer.start(1000, this);

            QPointer<RestManager> restManager = RestManager::getInstance();

            restManager->sendGet("script");
        } else if (path == "authentication/logout") {
            setCentralWidget(new DashboardForm);
            setModuleTitle("Dashboard");

            hide();

            RestManager::message(this, data, "Logout Success");

            loginDialog->show();
        }
    } else if (code == 400) {
        RestManager::message(this, data, "Bad Request");
    } else if (code == 404) {
        RestManager::message(this, data, "Not Found");
    } else if (code == 401) {
        // TODO: Bikin cara nanganin session timeout yg lebih bener
        if (!isSessionDone) {
            isSessionDone = true;

            RestManager::message(this, data, "Unauthorized");
            RestManager::getInstance()->sendGet("authentication/logout");

            isSessionDone = false;
        }
    }
}

void MainWindow::onCtiFeedbackReceived(QString feedback, bool error)
{
    ui->statusBar->showMessage(QString("CTI Client %1: %2").arg(error ? "Error" : "Feedback", feedback));
}

void MainWindow::on_actionLogout_triggered(bool checked)
{
    Q_UNUSED(checked)

    if (alertActivityChanged()) return;

    RestManager::getInstance()->sendGet("authentication/logout");
}

void MainWindow::on_actionDashboard_triggered(bool checked)
{
    Q_UNUSED(checked)

    if (alertActivityChanged()) return;

    if (dashboardForm.isNull()) {
        dashboardForm = new DashboardForm;
    }

    if (centralWidget() != dashboardForm) setCentralWidget(dashboardForm);

    setModuleTitle("Dashboard");
}

void MainWindow::on_actionSearchCustomers_triggered(bool checked)
{
    Q_UNUSED(checked)

    if (customerTabWidget.isNull()) {
        customerTabWidget = new CustomerTabWidget(userInfo);
    }

    if (centralWidget() != customerTabWidget) setCentralWidget(customerTabWidget);

    setModuleTitle("Search Customers");
}

void MainWindow::on_actionQualityAssurance_triggered(bool checked)
{
    Q_UNUSED(checked)

    if (alertActivityChanged()) return;

    if (qaTabWidget.isNull()) {
        qaTabWidget = new QATabWidget(userInfo);
    }

    if (centralWidget() != qaTabWidget) setCentralWidget(qaTabWidget);

    setModuleTitle("Quality Assurance");
}

void MainWindow::on_actionDataImport_triggered(bool checked)
{
    Q_UNUSED(checked)

    if (alertActivityChanged()) return;

    if (dataImportForm.isNull()) {
        dataImportForm = new DataImportForm(userInfo);
    }

    if (centralWidget() != dataImportForm) setCentralWidget(dataImportForm);

    setModuleTitle("Data Import");
}

void MainWindow::on_actionDataExport_triggered(bool checked)
{
    Q_UNUSED(checked)

    if (alertActivityChanged()) return;

    if (dataExportForm.isNull()) {
        dataExportForm = new DataExportForm(userInfo);
    }

    if (centralWidget() != dataExportForm) setCentralWidget(dataExportForm);

    setModuleTitle("Data Export");
}

void MainWindow::on_actionDistributeCustomer_triggered(bool checked)
{
    Q_UNUSED(checked)

    if (alertActivityChanged()) return;

    if (distributeForm.isNull()) {
        distributeForm = new DistributeForm(userInfo);
    }

    if (centralWidget() != distributeForm) setCentralWidget(distributeForm);

    setModuleTitle("Distribution");
}

void MainWindow::on_actionReport_triggered(bool checked)
{
    Q_UNUSED(checked)

    if (alertActivityChanged()) return;

    if (reportForm.isNull()) {
        reportForm = new ReportForm(userInfo);
    }

    if (centralWidget() != reportForm) setCentralWidget(reportForm);

    setModuleTitle("Reporting");
}

void MainWindow::on_actionChangePassword_triggered(bool checked)
{
    Q_UNUSED(checked)

    if (alertActivityChanged()) return;

    if (changePasswordForm.isNull()) {
        changePasswordForm = new ChangePasswordForm;
    }

    if (centralWidget() != changePasswordForm) setCentralWidget(changePasswordForm);

    setModuleTitle("Change Password");
}

void MainWindow::on_actionManageUser_triggered(bool checked)
{
    Q_UNUSED(checked)

    if (alertActivityChanged()) return;

    if (manageUserForm.isNull()) {
        manageUserForm = new ManageUserForm(userInfo);
    }

    if (centralWidget() != manageUserForm) setCentralWidget(manageUserForm);

    setModuleTitle("Manage User");
}
