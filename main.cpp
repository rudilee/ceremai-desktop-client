#include <QApplication>
#include <QtSingleApplication>
#include <QMessageBox>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationDomain("tmsholding.com");
    QCoreApplication::setOrganizationName("TMS");
    QCoreApplication::setApplicationName("Tele Marketing");
    QCoreApplication::setApplicationVersion("1.8");

    QtSingleApplication a(argc, argv);

    if (a.isRunning()) {
        QString title = QString("%1 %2").arg(QCoreApplication::applicationName(), QCoreApplication::applicationVersion());

        QMessageBox::critical(0, title, "One of the Application's instance already running!");

        return 0;
    }

    a.setActivationWindow(MainWindow::getInstance());

    return a.exec();
}
