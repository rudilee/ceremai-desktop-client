#ifndef DATEPICKERCALENDAR_H
#define DATEPICKERCALENDAR_H

#include <QDialog>

namespace Ui {
class DatePickerCalendar;
}

class DatePickerCalendar : public QDialog
{
    Q_OBJECT
    
public:
    explicit DatePickerCalendar(QWidget *parent = 0);
    ~DatePickerCalendar();
    
protected:
    void changeEvent(QEvent *e);
    
private:
    Ui::DatePickerCalendar *ui;
};

#endif // DATEPICKERCALENDAR_H
