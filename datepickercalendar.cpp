#include "datepickercalendar.h"
#include "ui_datepickercalendar.h"

DatePickerCalendar::DatePickerCalendar(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DatePickerCalendar)
{
    ui->setupUi(this);

    setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint);
}

DatePickerCalendar::~DatePickerCalendar()
{
    delete ui;
}

void DatePickerCalendar::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
