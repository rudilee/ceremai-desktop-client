#ifndef CUSTOMERTABWIDGET_H
#define CUSTOMERTABWIDGET_H

#include <QWidget>
#include <QMap>
#include <QPointer>

#include "searchdetailtab.h"
#include "customersearchmodel.h"
#include "customersearchform.h"

class CustomerTabWidget : public QWidget
{
    Q_OBJECT

private slots:
    void onCustomerTableClicked(QString customerId, QString customerCode);

public:
    explicit CustomerTabWidget(StringHash userInfo, QWidget *parent = 0);

    void closeEvent(QCloseEvent *event);

private:
    StringHash userInfo;
    QPointer<SearchDetailTab> tabWidget;
    QPointer<CustomerSearchModel> searchModel;
    QPointer<CustomerSearchForm> searchForm;

    void setupSearchForm();
};

#endif // CUSTOMERTABWIDGET_H
