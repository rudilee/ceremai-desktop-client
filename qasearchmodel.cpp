#include "qasearchmodel.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QPointer>
#include <QDate>
#include <QColor>

QASearchModel::QASearchModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    headers << "Row ID" << "Customer Code" << "Campaign" << "Name" << "Date of Birth" << "Created" << "Status" << "Product"
            << "Created By" << "Fullname";
    headersOrientation << 0 << 0 << 0 << 0 << 0 << 0 << 0 << 0;

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestreplyReceived(int,QString,QString, Http::Method,QByteArray)));
}

int QASearchModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return searchResult.size();
}

int QASearchModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return headers.count();
}

QVariant QASearchModel::data(const QModelIndex &index, int role) const
{
    if (index.isValid() && index.row() < searchResult.count()) {
        QStringList customerItem = searchResult.at(index.row());

        switch (role) {
        case Qt::DisplayRole:
            return customerItem.at(index.column());

            break;
        case Qt::ForegroundRole:
            QString status = customerItem.at(6);

            if (!status.isEmpty()) {
                if (status == "Approved") {
                    return QColor(0, 170, 0);
                } else if (status == "Reject") {
                    return QColor(170, 0, 0);
                } else {
                    return QColor(140, 140, 0);
                }
            }

            break;
        }
    }

    return QVariant();
}

QVariant QASearchModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            return QVariant(headers.at(section));
        } else if (orientation == Qt::Vertical) {
            return QVariant(++section);
        }
    }

    return QVariant();
}

Qt::ItemFlags QASearchModel::flags(const QModelIndex &index) const
{
    if (index.isValid()) {
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    }

    return 0;
}

QString QASearchModel::getCustomerId(QModelIndex index)
{
    return searchResult.value(index.row()).value(0);
}

QString QASearchModel::getCustomerCode(QModelIndex index)
{
    return searchResult.value(index.row()).value(1);
}

Qt::SortOrder QASearchModel::getHeaderOrientation(int logicalIndex)
{
    return headersOrientation.at(logicalIndex) == 1 ? Qt::AscendingOrder : Qt::DescendingOrder;
}

void QASearchModel::flipHeaderOrientation(int logicalIndex)
{
    if (headersOrientation.at(logicalIndex) == 0) {
        headersOrientation[logicalIndex] = 1;
    } else {
        headersOrientation[logicalIndex] = headersOrientation[logicalIndex] == 1 ? 2 : 1;
    }
}

void QASearchModel::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if  (path.startsWith("polis/page/")) {
            QVariantList customers = QJsonDocument::fromJson(data).array().toVariantList();

            // Hapus2in dulu row2 sebelumnya
            beginRemoveRows(QModelIndex(), 0, searchResult.size() - 1);
            endRemoveRows();

            searchResult.clear();

            beginInsertRows(QModelIndex(), 0, customers.size() - 1);
            foreach (QVariant customer, customers) {
                QVariantMap customerFields = customer.toMap();
                QStringList customerRow;

                customerRow << customerFields["id"].toString()
                            << customerFields["code"].toString()
                            << customerFields["campaign"].toString()
                            << customerFields["name"].toString()
                            << QDate::fromString(customerFields["date_of_birth"].toString(), "yyyy-MM-dd").toString("dd/MM/yyyy")
                            << customerFields["created"].toString()
                            << customerFields["status"].toString()
                            << customerFields["product"].toString()
                            << customerFields["username"].toString()
                            << customerFields["fullname"].toString();

                searchResult.append(customerRow);
            }
            endInsertRows();

            emit dataChanged(createIndex(0, 0), createIndex(searchResult.count() - 1, headers.count() - 1));
        }
    }
}
