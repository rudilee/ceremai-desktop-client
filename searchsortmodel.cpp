#include "searchsortmodel.h"

SearchSortModel::SearchSortModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{
}

void SearchSortModel::sort(int column, Qt::SortOrder order)
{
    this->sourceModel()->sort(column, order);
}
