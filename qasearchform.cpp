#include "qasearchform.h"
#include "ui_qasearchform.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

QASearchForm::QASearchForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QASearchForm)
{
    ui->setupUi(this);

    setupPolisSearch();
    setupPolisTable();

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestreplyReceived(int,QString,QString, Http::Method,QByteArray)));
}

QASearchForm::~QASearchForm()
{
    delete ui;
}

void QASearchForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void QASearchForm::setupPolisSearch()
{
    toggleSearchProgress(false);

    ui->statusSelect->addItem("");
    ui->statusSelect->addItem("Approved");
    ui->statusSelect->addItem("Reconfirm");
    ui->statusSelect->addItem("Reject");
    ui->statusSelect->addItem("Back Date");
    ui->statusSelect->addItem("Cancel");
    ui->statusSelect->addItem("Confirmed");

    ui->productSelect->addItem("");
    ui->productSelect->addItem("TLO Plus");
    ui->productSelect->addItem("All Risk");
    ui->productSelect->addItem("Malacca Speed");

    RestManager::getInstance()->sendGet("campaign");
}

void QASearchForm::setupPolisTable()
{
    searchModel = new QASearchModel;

    ui->polisTable->setModel(searchModel);
    ui->polisTable->hideColumn(0);
    ui->polisTable->horizontalHeader()->resizeSection(3, 250);
    ui->polisTable->horizontalHeader()->resizeSection(5, 120);
    ui->polisTable->horizontalHeader()->resizeSection(6, 100);
}

void QASearchForm::setupCampaignSelect(QByteArray data)
{
    QVariantList campaigns = QJsonDocument::fromJson(data).array().toVariantList();

    ui->campaignSelect->addItem("", 0);

    foreach (QVariant campaign, campaigns) {
        QVariantMap campaignFields = campaign.toMap();

        ui->campaignSelect->addItem(campaignFields["name"].toString(), campaignFields["id"]);
    }
}

void QASearchForm::setupCustomerDetail(QByteArray data)
{
    QVariantMap customer = QJsonDocument::fromJson(data).object().toVariantMap();

    toggleSearchProgress(false);
}

void QASearchForm::resetNavigationState()
{
    int currentPage = ui->currentPage->value(),
        lastPage = QVariant(ui->lastPage->text()).toInt();
    bool first = true, previous = true, next = true, last = true;

    if (currentPage <= 1) {
        first = false;
        previous = false;
    }

    if (currentPage >= lastPage) {
        next = false;
        last = false;
    }

    ui->firstButton->setEnabled(first);
    ui->previousButton->setEnabled(previous);
    ui->nextButton->setEnabled(next);
    ui->lastButton->setEnabled(last);
}

void QASearchForm::toggleSearchProgress(bool toggle)
{
    ui->searchProgress->setMaximum(toggle ? 0 : 1);
    ui->searchProgress->setValue(toggle ? -1 : 1);
    ui->searchProgress->setVisible(toggle);
}

void QASearchForm::retrieveCustomers()
{
    QString currentPageStr;
    int currentPage = ui->currentPage->value();

    if (searchCurrentPage == currentPage) return;

    searchCurrentPage = currentPage;

    if (currentPage == 0) {
        currentPageStr = "1";
    } else {
        currentPageStr = QVariant(currentPage).toString();
    }

    RestManager::getInstance()->sendGet(QString("customer/page/").append(currentPageStr), searchParams);

    toggleSearchProgress();
}

void QASearchForm::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if (path == "polis/product") {
        } else if (path == "polis/page") {
            QVariantMap pages = QJsonDocument::fromJson(data).object().toVariantMap();

            int lastPage = pages["last"].toInt(),
                currentPage = lastPage < 1 ? 0 : 1;

            ui->currentPage->setMinimum(currentPage);
            ui->currentPage->setMaximum(lastPage);
            ui->currentPage->setValue(currentPage);
            ui->lastPage->setText(pages["last"].toString());

            resetNavigationState();
        } else if (path.startsWith("polis/page/") || path.startsWith("customer/detail/")) {
            ui->polisTable->resizeColumnsToContents();

            toggleSearchProgress(false);
        } else if (path == "campaign") {
            setupCampaignSelect(data);
        }
    }
}

void QASearchForm::on_searchButton_clicked()
{
    QString name = ui->nameEdit->text(),
            phone = ui->phoneEdit->text(),
            code = ui->customerIdEdit->text(),
            campaign = ui->campaignSelect->itemData(ui->campaignSelect->currentIndex()).toString(),
            status = ui->statusSelect->currentText(),
            product = ui->productSelect->currentText();

    searchParams.clear();

    if (!name.isEmpty()) {
        searchParams.insert("name", name);
    }
    if (!phone.isEmpty()) {
        searchParams.insert("phone", phone);
    }
    if (!code.isEmpty()) {
        searchParams.insert("code", code);
    }
    if (campaign != "0") {
        searchParams.insert("campaign", campaign);
    }
    if (!status.isEmpty()) {
        searchParams.insert("status", status);
    }
    if (!product.isEmpty()) {
        searchParams.insert("product", product);
    }

    QPointer<RestManager> restManager = RestManager::getInstance();

    restManager->sendGet("polis/page", searchParams);
    restManager->sendGet("polis/page/1", searchParams);

    toggleSearchProgress();
}

void QASearchForm::on_clearButton_clicked()
{
    ui->nameEdit->clear();
    ui->phoneEdit->clear();
    ui->customerIdEdit->clear();
    ui->campaignSelect->setCurrentIndex(0);
    ui->statusSelect->setCurrentIndex(0);
    ui->productSelect->setCurrentIndex(0);
}

void QASearchForm::on_firstButton_clicked()
{
    ui->currentPage->setValue(ui->currentPage->minimum());

    retrieveCustomers();
    resetNavigationState();
}

void QASearchForm::on_previousButton_clicked()
{
    ui->currentPage->stepDown();

    retrieveCustomers();
    resetNavigationState();
}

void QASearchForm::on_nextButton_clicked()
{
    ui->currentPage->stepUp();

    retrieveCustomers();
    resetNavigationState();
}

void QASearchForm::on_lastButton_clicked()
{
    ui->currentPage->setValue(ui->currentPage->maximum());

    retrieveCustomers();
    resetNavigationState();
}

void QASearchForm::on_currentPage_editingFinished()
{
    retrieveCustomers();
    resetNavigationState();
}

void QASearchForm::on_polisTable_doubleClicked(const QModelIndex &index)
{
    QString customerId = searchModel->getCustomerId(index),
            customerCode = searchModel->getCustomerCode(index);

    if (!customerId.isEmpty()) {
        emit polisTableClicked(customerId, customerCode);

        toggleSearchProgress();
    }
}
