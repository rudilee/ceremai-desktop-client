#include "searchmodel.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QColor>

SearchModel::SearchModel(QString modelPath, QObject *parent) :
    QAbstractTableModel(parent),
    modelPath(modelPath)
{
    restManager = RestManager::getInstance();
    connect(restManager, SIGNAL(replyReceived(int,QString,QString,Http::Method,QByteArray)),
            this, SLOT(onRestReplyReceived(int,QString,QString,Http::Method,QByteArray)));
}

int SearchModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return searchResult.size();
}

int SearchModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return headers.count();
}

QVariant SearchModel::data(const QModelIndex &index, int role) const
{
    QVariant result;

    if (index.isValid() && index.row() < searchResult.count()) {
        QStringList item = searchResult.at(index.row());

        switch (role) {
        case Qt::ForegroundRole: result = foregroundRole(item);
            break;
        case Qt::TextAlignmentRole: result = textAlignmentRole(index.column());
            break;
        case Qt::DisplayRole:
            return item.at(index.column());

            break;
        }
    }

    return result;
}

QVariant SearchModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            return QVariant(headers.at(section));
        } else if (orientation == Qt::Vertical) {
            return QVariant(sequence + section);
        }
    }

    return QVariant();
}

Qt::ItemFlags SearchModel::flags(const QModelIndex &index) const
{
    if (index.isValid()) {
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    }

    return 0;
}

void SearchModel::sort(int column, Qt::SortOrder order)
{
    if (searchResult.isEmpty() || (this->column == column && this->order == order)) return;

    this->column = column;
    this->order = order;

    emit columnSorting(fields.at(column), (order == Qt::AscendingOrder ? "ASC" : "DESC"));
}

void SearchModel::setHeaders(QStringList headers)
{
    this->headers = headers;
}

void SearchModel::setFields(QStringList fields)
{
    this->fields = fields;
}

void SearchModel::setParseRow(QStringList (*parseRow)(QVariantMap))
{
    this->parseRow = parseRow;
}

void SearchModel::setDataRole(QVariant (*foregroundRole)(QStringList), QVariant (*textAlignmentRole)(int))
{
    this->foregroundRole = foregroundRole;
    this->textAlignmentRole = textAlignmentRole;
}

QString SearchModel::getColumnValue(QModelIndex index, int position)
{
    return searchResult.value(index.row()).value(position);
}

void SearchModel::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    Q_UNUSED(method)

    if (contentType != "application/json") return;

    if (code == 200) {
        if  (path == modelPath) {
            QVariantMap response = QJsonDocument::fromJson(data).object().toVariantMap();
            QVariantList results = response["results"].toList();

            pageNumber = response["page"].toInt();
            sequence = (pageNumber - 1) * response["per_page"].toInt() + 1;

            // Hapus2in dulu row2 sebelumnya
            beginRemoveRows(QModelIndex(), 0, searchResult.size() - 1);
            endRemoveRows();

            searchResult.clear();

            beginInsertRows(QModelIndex(), 0, results.size() - 1);

            foreach (QVariant result, results) {
                searchResult.append(parseRow(result.toMap()));
            }

            endInsertRows();

            emit dataChanged(createIndex(0, 0), createIndex(searchResult.count() - 1, headers.count() - 1));
        }
    }
}
