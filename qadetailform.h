#ifndef QADETAILFORM_H
#define QADETAILFORM_H

#include <QWidget>
#include <QModelIndex>
#include <QPointer>

#include "restmanager.h"

namespace Ui {
    class QADetailForm;
}

class QADetailForm : public QWidget
{
    Q_OBJECT

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

public:
    explicit QADetailForm(QString customerId, QWidget *parent = 0);
    ~QADetailForm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::QADetailForm *ui;

    QString customerId;

    void setupCallActivity();
    void getFormDetails();
    void setupCustomerDetail(QByteArray data);
    void setupCallActivityHistory(QByteArray data);
};

#endif // QADETAILFORM_H
