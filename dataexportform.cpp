#include "dataexportform.h"
#include "ui_dataexportform.h"

#include <QFileDialog>
#include <QMessageBox>

DataExportForm::DataExportForm(StringHash userInfo, QWidget *parent) :
    userInfo(userInfo),
    QWidget(parent),
    ui(new Ui::DataExportForm)
{
    ui->setupUi(this);

    dataExportModel = new DataExportModel;
    ui->dataExportTable->setModel(dataExportModel);

    setupOkFileTab();
    setupRecurringTab();
    toggleRetrieveProgress(false);

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestReplyReceived(int,QString,QString, Http::Method,QByteArray)));
}

DataExportForm::~DataExportForm()
{
    delete ui;
}

void DataExportForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void DataExportForm::setupOkFileTab()
{
    QDate currentDate = QDate::currentDate();
    int year = currentDate.year(),
        month = currentDate.month();

    ui->okFileCreatedFrom->setDate(QDate(year, month, 1));
    ui->okFileCreatedTo->setDate(QDate(year, month, currentDate.daysInMonth()));

    ui->okFileProduct->addItem("Life Protector Plus", "lpp");
//    ui->okFileProduct->addItem("Mobile Vehicle", "mv");

    ui->polisStatus->addItem("");
    ui->polisStatus->addItem("Approved");
    ui->polisStatus->addItem("Reconfirm");
    ui->polisStatus->addItem("Reject");
    ui->polisStatus->addItem("Back Date");
    ui->polisStatus->addItem("Cancel");
    ui->polisStatus->addItem("Confirmed");
}

void DataExportForm::setupRecurringTab()
{
    QDate currentDate = QDate::currentDate();
    int year = currentDate.year(),
        month = currentDate.month();

    ui->recurringCreatedFrom->setDate(QDate(year, month, 1));
    ui->recurringCreatedTo->setDate(QDate(year, month, currentDate.daysInMonth()));

    ui->recurringProduct->addItem("Life Protector Plus", "lpp");
//    ui->recurringProduct->addItem("Mobile Vehicle", "mv");
}

void DataExportForm::setupTableHeaders()
{
    QStringList headers;

    switch (ui->exportTabs->currentIndex()) {
    case 0: // OK File
        switch (ui->okFileProduct->currentIndex()) {
        case 0: // Life Protector Plus
            headers << "no" << "recordnr" << "cif" << "tarp" << "email" << "rel1" << "rel2" << "rel3"
                    << "cardtype" << "cardprefix" << "agent" << "callresult" << "callstatus" << "rejectrsn"
                    << "card_number_completed_diff_requested_prefix" << "addr1" << "addr2" << "addr3" << "city" << "zip"
                    << "mobile" << "office" << "office2" << "home" << "fax" << "plan" << "premi" << "benef1" << "benef2"
                    << "benef3" << "salesdate" << "title" << "total_member" << "dob_1" << "name_1" << "sex_1" << "rel_1"
                    << "kind_of_disease_1" << "dob_2" << "name_2" << "sex_2" << "rel_2" << "expired_date"
                    << "agent_name" << "spv_name" << "atm_name";

            break;
        case 1: // Mobile Vehicle
            headers << "";

            break;
        }

        break;
    case 1: // Recurring
        switch (ui->recurringProduct->currentIndex()) {
        case 0: // Life Protector Plus
            headers << "Nomor_Register" << "Nomor_Referensi" << "Nomor_Kartu_Kredit"
                    << "Masa_Berlaku_Kredit" << "Jumlah_Transaksi" << "Decimal_Point" << "Nama_Pemilik_Kartu"
                    << "Description" << "Filler" << "Alamat_Pemilik_Kartu" << "Nomor_Telephone" << "Periode"
                    << "agent_name" << "spv_name" << "atm_name";

            break;
        case 1: // Mobile Vehicle
            headers << "INSURED" << "INCEPTION" << "EXPIRY" << "REFNO" << "NO_OF_RISK"
                    << "CONJ_POLICYNO" << "CONJ_CERTIFICATENO" << "OBJ_INFO_01" << "OBJ_INFO_02" << "OBJ_INFO_03"
                    << "OBJ_INFO_04" << "OBJ_INFO_05" << "OBJ_INFO_06" << "OBJ_INFO_07" << "OBJ_INFO_08"
                    << "OBJ_INFO_09" << "OBJ_INFO_10" << "OBJ_INFO_11" << "OBJ_INFO_12" << "OBJ_INFO_13"
                    << "OBJ_INFO_14" << "OBJ_INFO_15" << "RISKLOCATIONREMARK" << "MAINPCTADJ" << "ADDINTERESTCODE1"
                    << "ADDINTERESTREMARK1" << "ADDCURRENCY1" << "ADDSUMINSURED1" << "ADDPCTADJ1"
                    << "ADDINTERESTCODE2" << "ADDINTERESTREMARK2" << "ADDCURRENCY2" << "ADDSUMINSURED2"
                    << "ADDPCTADJ2" << "ADDINTERESTCODE3" << "ADDINTERESTREMARK3" << "ADDCURRENCY3"
                    << "ADDSUMINSURED3" << "ADDPCTADJ3" << "ADDINTERESTCODE4" << "ADDINTERESTREMARK4"
                    << "ADDCURRENCY4" << "ADDSUMINSURED4" << "ADDPCTADJ4" << "ADDINTERESTCODE5"
                    << "ADDINTERESTREMARK5" << "ADDCURRENCY5" << "ADDSUMINSURED5" << "ADDPCTADJ5" << "PRODUCT"
                    << "TOPRO_CURRENCY" << "TOPRO_MAINSUMINSURED" << "DISCOUNT" << "INSTALLMENT_FREQ"
                    << "INSTALLMENT_PERIOD" << "INSTALLMENT_FIRSTDUE";
        }

        break;
    }

    dataExportModel->setHeaders(headers);
}

void DataExportForm::toggleRetrieveProgress(bool toggle)
{
    ui->retrieveProgress->setMaximum(toggle ? 0 : 1);
    ui->retrieveProgress->setValue(toggle ? -1 : 1);
    ui->retrieveProgress->setVisible(toggle);
}

QString DataExportForm::getExportPath()
{
    QString typePath, productPath;

    switch (ui->exportTabs->currentIndex()) {
    case 0: // OK File
        typePath = "ok_file";
        productPath = ui->okFileProduct->itemData(ui->okFileProduct->currentIndex()).toString();

        break;
    case 1: // Recurring
        typePath = "recurring";
        productPath = ui->recurringProduct->itemData(ui->recurringProduct->currentIndex()).toString();

        break;
    }

    return QString("%1/%2").arg(typePath, productPath);
}

StringHash DataExportForm::getExportParams()
{
    StringHash params = StringHash();
    QString createdFrom, createdTo, polisStatus;

    switch (ui->exportTabs->currentIndex()) {
    case 0: // OK File
        createdFrom = ui->okFileCreatedFrom->date().toString("yyyy-MM-dd");
        createdTo = ui->okFileCreatedTo->date().toString("yyyy-MM-dd");
        polisStatus = ui->polisStatus->currentText();

        break;
    case 1: // Recurring
        createdFrom = ui->recurringCreatedFrom->date().toString("yyyy-MM-dd");
        createdTo = ui->recurringCreatedTo->date().toString("yyyy-MM-dd");

        break;
    }

    if (createdFrom != "2000-01-01") {
        params.insert("created_from", createdFrom);
    }
    if (createdTo != "2000-01-01") {
        params.insert("created_to", createdTo);
    }
    if (!polisStatus.isEmpty()) {
        params.insert("status", polisStatus);
    }

    return params;
}

void DataExportForm::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if (path.startsWith("export/recurring") || path.startsWith("export/ok_file")) {
            toggleRetrieveProgress(false);
        } else if (path.startsWith("export/excel")) {
            QString productName = getExportPath().replace("/", "_").toUpper();
            QString currentDate = QDateTime::currentDateTime().toString("hhmmssddMMyyyy");
            QString filename = QFileDialog::getSaveFileName(this, "Save Recurring File",
                                                            QString("%1/%2_%3.xls").arg(QDir::homePath(), productName, currentDate),
                                                            "Microsoft Excel 5.0 (*.xls)");
            if (filename.isEmpty()) return;

            QFile file(filename);

            if (!file.open(QIODevice::WriteOnly)) {
                QMessageBox::critical(this, "Save Error", "Membuka file untuk menyimpan gagal");

                return;
            }

            if(file.write(data) == -1) {
                QMessageBox::critical(this, "Save Error", "Menyimpan data ke file tujuan gagal");
            }
        }
    }
}

void DataExportForm::on_retrieveResult_clicked()
{
    setupTableHeaders();
    toggleRetrieveProgress();

    QString exportPath = getExportPath().prepend("export/");

    dataExportModel->setRetrievePath(exportPath);

    RestManager::getInstance()->sendGet(exportPath, getExportParams());
}

void DataExportForm::on_exportResult_clicked()
{
    RestManager::getInstance()->sendGet(getExportPath().prepend("export/excel/"), getExportParams());
}
