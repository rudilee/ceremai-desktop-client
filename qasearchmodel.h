#ifndef QASEARCHMODEL_H
#define QASEARCHMODEL_H

#include <QAbstractTableModel>
#include <QStringList>

#include "restmanager.h"

class QASearchModel : public QAbstractTableModel
{
    Q_OBJECT

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

public:
    explicit QASearchModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    QString getCustomerId(QModelIndex index);
    QString getCustomerCode(QModelIndex index);
    Qt::SortOrder getHeaderOrientation(int logicalIndex);
    void flipHeaderOrientation(int logicalIndex);

private:
    QStringList headers;
    QList<int> headersOrientation;
    QList<QStringList> searchResult;
};

#endif // QASEARCHMODEL_H
