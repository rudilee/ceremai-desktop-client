#include "customersearchmodel.h"

#include <QPointer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QDate>
#include <QDateTime>
#include <QColor>

CustomerSearchModel::CustomerSearchModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    headers << "Row ID" << "Customer Code" << "Campaign" << "Agent" << "Name" << "Date of Birth"
            << "Home Phone" << "Office Phone" << "Mobile Phone"
            << "Call Disposition" << "Call Date" << "Remarks" << "Product Status";
    headersOrientation << 0 << 0 << 0 << 0 << 0 << 0 << 0 << 0 << 0 << 0 << 0;

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestReplyReceived(int,QString,QString, Http::Method,QByteArray)));
}

int CustomerSearchModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return searchResult.size();
}

int CustomerSearchModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return headers.count();
}

QVariant CustomerSearchModel::data(const QModelIndex &index, int role) const
{
    if (index.isValid() && index.row() < searchResult.count()) {
        QStringList customerItem = searchResult.at(index.row());

        switch (role) {
        case Qt::ForegroundRole:
            if (!customerItem.at(9).isEmpty()) {
                return QColor(0, 0, 170);
            }

            break;
        case Qt::TextAlignmentRole:
            switch (index.column()) {
            case 5:
            case 10:
                return Qt::AlignCenter;

                break;
            }

            break;
        case Qt::DisplayRole:
            return customerItem.at(index.column());

            break;
        }
    }

    return QVariant();
}

QVariant CustomerSearchModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            return QVariant(headers.at(section));
        } else if (orientation == Qt::Vertical) {
            return QVariant(++section);
        }
    }

    return QVariant();
}

Qt::ItemFlags CustomerSearchModel::flags(const QModelIndex &index) const
{
    if (index.isValid()) {
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    }

    return 0;
}

QString CustomerSearchModel::getCustomerId(QModelIndex index)
{
    return searchResult.value(index.row()).value(0);
}

QString CustomerSearchModel::getCustomerCode(QModelIndex index)
{
    return searchResult.value(index.row()).value(1);
}

bool CustomerSearchModel::getCustomerDetail(QModelIndex index)
{
    QString customerId = searchResult.value(index.row()).value(0);

    if (customerId.isEmpty()) return false;

    QPointer<RestManager> restManager = RestManager::getInstance();

    restManager->sendGet(QString("customer/detail/").append(customerId));
    restManager->sendGet(QString("call_activity/history/").append(customerId));

    return true;
}

Qt::SortOrder CustomerSearchModel::getHeaderOrientation(int logicalIndex)
{
    return headersOrientation.at(logicalIndex) == 1 ? Qt::AscendingOrder : Qt::DescendingOrder;
}

void CustomerSearchModel::flipHeaderOrientation(int logicalIndex)
{
    if (headersOrientation.at(logicalIndex) == 0) {
        headersOrientation[logicalIndex] = 1;
    } else {
        headersOrientation[logicalIndex] = headersOrientation[logicalIndex] == 1 ? 2 : 1;
    }
}

void CustomerSearchModel::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if  (path.startsWith("customer/page/")) {
            QVariantList customers = QJsonDocument::fromJson(data).array().toVariantList();

            // Hapus2in dulu row2 sebelumnya
            beginRemoveRows(QModelIndex(), 0, searchResult.size() - 1);
            endRemoveRows();

            searchResult.clear();

            beginInsertRows(QModelIndex(), 0, customers.size() - 1);
            foreach (QVariant customer, customers) {
                QVariantMap customerFields = customer.toMap();
                QStringList customerRow;

                customerRow << customerFields["id"].toString()
                            << customerFields["code"].toString()
                            << customerFields["campaign"].toString()
                            << customerFields["agent"].toString()
                            << customerFields["name"].toString()
                            << QDate::fromString(customerFields["date_of_birth"].toString(), "yyyy-MM-dd").toString("dd/MM/yyyy")
                            << customerFields["home_phone"].toString()
                            << customerFields["office_phone"].toString()
                            << customerFields["mobile_phone"].toString()
                            << customerFields["call_disposition"].toString()
                            << QDateTime::fromString(customerFields["call_date"].toString(), "yyyy-MM-dd hh:mm:ss").toString("dd/MM/yyyy")
                            << customerFields["remarks"].toString()
                            << customerFields["product_status"].toString();


                searchResult.append(customerRow);
            }
            endInsertRows();

            emit dataChanged(createIndex(0, 0), createIndex(searchResult.count() - 1, headers.count() - 1));
        }
    }
}
