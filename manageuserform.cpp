#include "manageuserform.h"
#include "ui_manageuserform.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>
#include <QPointer>

ManageUserForm::ManageUserForm(StringHash userInfo, QWidget *parent) :
    userInfo(userInfo),
    QWidget(parent),
    ui(new Ui::ManageUserForm)
{
    ui->setupUi(this);

    state = Idle;

    RestManager::getInstance()->sendGet("user/role");

    setupUsersTable();

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestReplyReceived(int,QString,QString, Http::Method,QByteArray)));
}

ManageUserForm::~ManageUserForm()
{
    delete ui;
}

void ManageUserForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void ManageUserForm::setupUsersTable()
{
    QStringList labels;
    labels << "Row ID" << "Username" << "First Name" << "Last Name" << "Role";

    ui->usersTable->setHorizontalHeaderLabels(labels);
    ui->usersTable->hideColumn(0);
    ui->usersTable->horizontalHeader()->resizeSection(1, 150);
    ui->usersTable->horizontalHeader()->resizeSection(2, 200);
    ui->usersTable->horizontalHeader()->resizeSection(3, 200);
    ui->usersTable->horizontalHeader()->resizeSection(4, 150);
}

void ManageUserForm::setupUserRole(QByteArray data)
{
    QVariantList roles = QJsonDocument::fromJson(data).array().toVariantList();

    ui->role->addItem("", 0);

    foreach (QVariant role, roles) {
        QVariantMap roleFields = role.toMap();

        QStringList roleItem;
        roleItem << roleFields["name"].toString() << roleFields["label"].toString();
        userRole.insert(roleFields["id"].toInt(), roleItem);

        ui->role->addItem(QIcon(QString(":/combo/").append(roleItem[0])), roleItem[1], roleFields["id"]);
    }

    refreshUserData();
}

void ManageUserForm::setupSuperior(QByteArray data)
{
    QVariantList users = QJsonDocument::fromJson(data).array().toVariantList();

    ui->superior->clear();
    ui->superior->addItem("", 0);

    foreach (QVariant user, users) {
        QVariantMap userFields = user.toMap();
        QString name = QString("%1\t%2 %3").arg(userFields["username"].toString(), userFields["first_name"].toString(),
                                                userFields["last_name"].toString());

        ui->superior->addItem(name, userFields["id"]);
    }
}

void ManageUserForm::populateUsersTable(QByteArray data)
{
    QVariantList users = QJsonDocument::fromJson(data).array().toVariantList();

    ui->usersTable->clearContents();

    int row = 0;
    foreach (QVariant user, users) {
        QVariantMap userFields = user.toMap();

        ui->usersTable->setRowCount(row + 1);

        ui->usersTable->setItem(row, 0, new QTableWidgetItem(userFields["id"].toString()));
        ui->usersTable->setItem(row, 1, new QTableWidgetItem(userFields["username"].toString()));
        ui->usersTable->setItem(row, 2, new QTableWidgetItem(userFields["first_name"].toString()));
        ui->usersTable->setItem(row, 3, new QTableWidgetItem(userFields["last_name"].toString()));

        QStringList role = userRole.value(userFields["role_id"].toInt());
        ui->usersTable->setItem(row, 4, new QTableWidgetItem(QIcon(QString(":/combo/").append(role[0])), role[1]));

        row++;
    }
}

void ManageUserForm::fillUserFields(QByteArray data)
{
    QVariantMap user = QJsonDocument::fromJson(data).object().toVariantMap();

    ui->username->setText(user["username"].toString());
    ui->firstName->setText(user["first_name"].toString());
    ui->lastName->setText(user["last_name"].toString());
    ui->role->setCurrentIndex(ui->role->findData(user["role_id"]));
    ui->superior->setCurrentIndex(ui->superior->findData(user["parent_user_id"]));
}

void ManageUserForm::changeState(State state)
{
    bool addUser = false, editUser = false, deleteUser = false,
         save = false, cancel = false,
         usersTable = true,
         userFields = true;

    this->state = state;

    switch (state) {
    case Adding:
        clearUserFields();
    case Editing:
        save = true;
        cancel = true;
        usersTable = false;
        userFields = false;

        break;
    case Selecting:
    case Idle:
        addUser = true;
        if (!ui->usersTable->selectedItems().isEmpty()) {
            this->state = Selecting;

            editUser = true;
            deleteUser = true;
        } else {
            clearUserFields();
        }
    }

    ui->addUser->setEnabled(addUser);
    ui->editUser->setEnabled(editUser);
    ui->deleteUser->setEnabled(deleteUser);
    ui->save->setEnabled(save);
    ui->cancel->setEnabled(cancel);
    ui->usersTable->setEnabled(usersTable);
    ui->username->setReadOnly(userFields);
    ui->password->setReadOnly(userFields);
    ui->passwordVerify->setReadOnly(userFields);
    ui->firstName->setReadOnly(userFields);
    ui->lastName->setReadOnly(userFields);
}

void ManageUserForm::clearUserFields()
{
    ui->username->clear();
    ui->password->clear();
    ui->passwordVerify->clear();
    ui->firstName->clear();
    ui->lastName->clear();
    ui->role->setCurrentIndex(0);
    ui->superior->setCurrentIndex(0);
    ui->usersTable->clearSelection();
}

void ManageUserForm::toggleRequiredField(bool update)
{
    QString styleSheet = "color: rgb(170, 0, 0);";

    if (update) styleSheet = "";

    ui->passwordLabel->setStyleSheet(styleSheet);
    ui->passwordVerifyLabel->setStyleSheet(styleSheet);
}

void ManageUserForm::refreshUserData()
{
    QPointer<RestManager> restManager = RestManager::getInstance();

    restManager->sendGet("user/superior");
    restManager->sendGet("user");
}

void ManageUserForm::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if (path == "user") {
            populateUsersTable(data);
        } else if(path == "user/role") {
            setupUserRole(data);
        } else if(path == "user/superior") {
            setupSuperior(data);
        } else if(path.startsWith("user/detail/")) {
            fillUserFields(data);
        } else if(path.startsWith("user/update/")) {
            refreshUserData();

            RestManager::message(this, data, "Update User");
        } else if(path == "user/new") {
            refreshUserData();

            RestManager::message(this, data, "Add User");
        } else if(path.startsWith("user/delete/")) {
            refreshUserData();

            RestManager::message(this, data, "Delete User");
        }
    }
}

void ManageUserForm::on_addUser_clicked()
{
    toggleRequiredField();
    changeState(Adding);
}

void ManageUserForm::on_editUser_clicked()
{
    toggleRequiredField(true);
    changeState(Editing);
}

void ManageUserForm::on_deleteUser_clicked()
{
    ui->usersTable->clearSelection();

    RestManager::getInstance()->sendDelete(QString("user/delete/").append(currentUserId));

    changeState(Idle);
}

void ManageUserForm::on_save_clicked()
{
    QString username = ui->username->text(),
            password = ui->password->text(),
            passwordVerify = ui->passwordVerify->text(),
            firstName = ui->firstName->text(),
            lastName = ui->lastName->text();

    int roleId = ui->role->itemData(ui->role->currentIndex()).toInt(),
        parentUserId = ui->superior->itemData(ui->superior->currentIndex()).toInt();

    bool emptyFields = false;

    if (state == Adding) {
        if (username.isEmpty() || password.isEmpty() || passwordVerify.isEmpty()) emptyFields = true;
    }

    if (firstName.isEmpty() || roleId == 0) emptyFields = true;

    if (emptyFields) {
        QMessageBox::warning(this, "Empty Fields", "Field mandatori tidak boleh dikosongkan");

        return;
    }
    if (password != passwordVerify) {
        QMessageBox::warning(this, "Password Verify", "Field Password dan verifikasinya tidak sama");

        return;
    }
    if (parentUserId == currentUserId.toInt()) {
        QMessageBox::warning(this, "User Superior", "Tidak bisa menjadi atasan diri sendiri");

        return;
    }

    StringHash params = StringHash();

    if (state == Adding) {
        params.insert("username", username);
    }

    params.insert("password", password);
    params.insert("password_verify", passwordVerify);
    params.insert("first_name", firstName);

    if (!lastName.isEmpty()) {
        params.insert("last_name", lastName);
    }

    params.insert("role_id", QString::number(roleId));

    if (parentUserId != 0) {
        params.insert("parent_user_id", QString::number(parentUserId));
    }

    if (state == Editing) {
        RestManager::getInstance()->sendPut(QString("user/update/").append(currentUserId), params);
    } else if (state == Adding) {
        RestManager::getInstance()->sendPost("user/new", params);
    }

    ui->usersTable->clearSelection();

    changeState(Idle);
}

void ManageUserForm::on_cancel_clicked()
{
    ui->password->clear();
    ui->passwordVerify->clear();

    toggleRequiredField();
    changeState(Idle);
}

void ManageUserForm::on_usersTable_clicked(QModelIndex index)
{
    currentUserId = ui->usersTable->item(index.row(), 0)->text();

    RestManager::getInstance()->sendGet(QString("user/detail/").append(currentUserId));

    changeState(Selecting);
}
