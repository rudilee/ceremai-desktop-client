#ifndef DATEPICKERINPUT_H
#define DATEPICKERINPUT_H

#include <QLineEdit>
#include <QPointer>
#include <QCalendarWidget>

//#include "datepickercalendar.h"

namespace Ui {
class DatePickerInput;
}

class DatePickerInput : public QLineEdit
{
    Q_OBJECT
    
public:
    explicit DatePickerInput(QWidget *parent = 0);
    ~DatePickerInput();
    
protected:
    void changeEvent(QEvent *e);

    virtual void focusInEvent(QFocusEvent *e);
    virtual void focusOutEvent(QFocusEvent *e);
    
private:
    Ui::DatePickerInput *ui;

    QCalendarWidget calendarPopup;
};

#endif // DATEPICKERINPUT_H
