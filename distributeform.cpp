#include "distributeform.h"
#include "ui_distributeform.h"

#include <QPointer>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QTableWidgetItem>

DistributeForm::DistributeForm(StringHash userInfo, QWidget *parent) :
    userInfo(userInfo),
    QWidget(parent),
    ui(new Ui::DistributeForm)
{
    QPointer<RestManager> restManager = RestManager::getInstance();

    ui->setupUi(this);

    setupUsersTable();

    connect(restManager, SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestReplyReceived(int,QString,QString, Http::Method,QByteArray)));

    restManager->sendGet("user/role");
    restManager->sendGet("distribution/campaign");
}

DistributeForm::~DistributeForm()
{
    delete ui;
}

void DistributeForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void DistributeForm::setupUsersTable()
{
    QStringList labels;
    labels << "Id" << "Username" << "Fullname" << "Role" << "Quota" << "Utilised" << "Not Utilised";

    ui->usersTable->setHorizontalHeaderLabels(labels);
    ui->usersTable->setColumnWidth(0, 50);
    ui->usersTable->setColumnWidth(1, 150);
    ui->usersTable->setColumnWidth(2, 200);
    ui->usersTable->setColumnWidth(3, 150);
}

void DistributeForm::refreshDistributeForm()
{
    QString campaignId = ui->campaignCombo->itemData(ui->campaignCombo->currentIndex()).toString();

    QPointer<RestManager> restManager = RestManager::getInstance();

    restManager->sendGet(QString("distribution/quota/").append(campaignId));
    restManager->sendGet(QString("distribution/user/").append(campaignId));
}

void DistributeForm::setupUserRole(QByteArray data)
{
    QVariantList roles = QJsonDocument::fromJson(data).array().toVariantList();

    foreach (QVariant role, roles) {
        QVariantMap roleFields = role.toMap();

        QStringList roleItem;
        roleItem << roleFields["name"].toString() << roleFields["label"].toString();
        userRole.insert(roleFields["id"].toInt(), roleItem);
    }

    RestManager::getInstance()->sendGet("distribution/user");
}

void DistributeForm::setupCampaignSelect(QByteArray data)
{
    QVariantList campaigns = QJsonDocument::fromJson(data).array().toVariantList();

    ui->campaignCombo->addItem("");

    foreach (QVariant campaign, campaigns) {
        QVariantMap campaignFields = campaign.toMap();

        ui->campaignCombo->addItem(campaignFields["name"].toString(), campaignFields["id"]);
    }
}

void DistributeForm::populateUsersTable(QByteArray data)
{
    QVariantList users = QJsonDocument::fromJson(data).array().toVariantList();

    ui->usersTable->clearContents();

    int row = 0;
    foreach (QVariant user, users) {
        QVariantMap userFields = user.toMap();

        QTableWidgetItem *check = new QTableWidgetItem(userFields["id"].toString());
        check->setCheckState(Qt::Unchecked);

        ui->usersTable->setRowCount(row + 1);
        ui->usersTable->setItem(row, 0, check);
        ui->usersTable->setItem(row, 1, new QTableWidgetItem(userFields["username"].toString()));
        ui->usersTable->setItem(row, 2, new QTableWidgetItem(QString("%1 %2").arg(userFields["first_name"].toString(),
                                                                                userFields["last_name"].toString())));

        QStringList role = userRole.value(userFields["role_id"].toInt());
        ui->usersTable->setItem(row, 3, new QTableWidgetItem(QIcon(QString(":/combo/").append(role[0])), role[1]));

        int quota = userFields["quota"].toInt();
        int utilised = userFields["utilised"].toInt();
        int notUtilised = quota - utilised;

        ui->usersTable->setItem(row, 4, new QTableWidgetItem(QString::number(quota)));
        ui->usersTable->setItem(row, 5, new QTableWidgetItem(QString::number(utilised)));
        ui->usersTable->setItem(row, 6, new QTableWidgetItem(QString::number(notUtilised)));

        row++;
    }
}

void DistributeForm::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if(path == "user/role") {
            setupUserRole(data);
        } else if (path.startsWith("distribution/user")) {
            populateUsersTable(data);
        } else if (path == "distribution/campaign") {
            setupCampaignSelect(data);
        } else if (path.startsWith("distribution/quota/")) {
            QVariantMap quota = QJsonDocument::fromJson(data).object().toVariantMap();

            ui->dataQuota->setText(quota["quota"].toString());
            ui->dataAllocation->setMaximum(quota["quota"].toInt());
        } else if (path == "distribution/transfer" || path == "distribution/withdraw") {
            QVariantMap message = QJsonDocument::fromJson(data).object().toVariantMap();

            QMessageBox::information(this, "Distribution", message["message"].toString());

            refreshDistributeForm();
        }
    }
}

void DistributeForm::on_campaignCombo_activated(int index)
{
    Q_UNUSED(index)

    refreshDistributeForm();
}

void DistributeForm::on_transferData_clicked()
{
    StringHash params = StringHash();
    int campaignIndex = ui->campaignCombo->currentIndex();
    QString campaign_id = campaignIndex == -1 ? "" : ui->campaignCombo->itemData(ui->campaignCombo->currentIndex()).toString(),
            allocation = ui->dataAllocation->text();

    if (campaign_id.isEmpty() && allocation.isEmpty()) return;

    for (int i = 0; i < ui->usersTable->rowCount(); i++) {
        if (ui->usersTable->item(i, 0)->checkState() == Qt::Checked) {
            params.insertMulti("user_ids[]", ui->usersTable->item(i, 0)->text());
        }
    }

    if (params.isEmpty()) return;

    params["campaign_id"] = campaign_id;
    params["allocation"] = allocation;

    RestManager::getInstance()->sendPut("distribution/transfer", params);
}

void DistributeForm::on_withdrawData_clicked()
{
    StringHash params = StringHash();
    int campaignIndex = ui->campaignCombo->currentIndex();
    QString campaign_id = campaignIndex == -1 ? "" : ui->campaignCombo->itemData(ui->campaignCombo->currentIndex()).toString();

    if (campaign_id.isEmpty()) return;

    for (int i = 0; i < ui->usersTable->rowCount(); i++) {
        if (ui->usersTable->item(i, 0)->checkState() == Qt::Checked) {
            params.insertMulti("user_ids[]", ui->usersTable->item(i, 0)->text());
        }
    }

    if (params.isEmpty()) return;

    params["campaign_id"] = campaign_id;

    RestManager::getInstance()->sendPut("distribution/withdraw", params);
}
