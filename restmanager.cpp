#include "restmanager.h"

#include <QApplication>
#include <QUrl>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>
#include <QDebug>
#include <QSslCertificate>
#include <QSslConfiguration>

RestManager *RestManager::instance = NULL;

RestManager::RestManager(QObject *parent) :
    QNetworkAccessManager(parent)
{
    userAgent = QString("%1 %2").arg(QApplication::applicationName(), QApplication::applicationVersion()).toLatin1();
    methodList << "UNKNOWN" << "HEAD" << "GET" << "PUT" << "POST" << "DELETE" << "CUSTOM";

    connect(this, SIGNAL(finished(QNetworkReply*)), SLOT(onRequestFinished(QNetworkReply*)));
}

QNetworkRequest RestManager::setupRequest()
{
//    QSslConfiguration sslConfiguration;
//    sslConfiguration.setCaCertificates(QSslCertificate::fromPath("tmsis.cer"));

    QNetworkRequest request;
//    request.setSslConfiguration(sslConfiguration);

    return request;
}

RestManager *RestManager::getInstance()
{
    if (instance == NULL) instance = new RestManager;

    return instance;
}

void RestManager::onRequestFinished(QNetworkReply *reply)
{
    if (reply->isFinished()) {
        int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        QString path = reply->url().path().replace(appPath, ""),
                // TODO: hack sementara bat nanganin header Content-Type yg ada parameter charset nya..
                contentType = reply->header(QNetworkRequest::ContentTypeHeader).toString().replace("; charset=utf-8", "");
        QByteArray data = reply->readAll();
        QNetworkAccessManager::Operation operation = reply->operation();

        qDebug() << QString("[%5 %1 (%3) <%4>: %2]\n").arg(path, data.data(), QString::number(code), contentType, methodList.at((int) operation));

        emit replyReceived(code, path, contentType, static_cast<Http::Method> (operation), data);

        if (code >= 500) {
            QMessageBox error;

            error.setWindowTitle("Server Error");
            error.setIcon(QMessageBox::Critical);
            error.setText("<b>Telah terjadi kesalahan pada server</b>");
            error.setInformativeText("Silahkan lihat pada details...");
            error.setDetailedText(QString("Path: %1\nReply:\n%2").arg(path, data.data()));
            error.exec();
        }
    }

    reply->deleteLater();
}

void RestManager::setBaseUrl(QString baseUrl)
{
//    if (!baseUrl.startsWith("https://")) baseUrl.prepend("https://");
    if (!baseUrl.startsWith("http://")) baseUrl.prepend("http://");

//    if (baseUrl.endsWith("/")) baseUrl.chop(1);
    if (!baseUrl.endsWith("/")) baseUrl.append("/");

    this->baseUrl = baseUrl;
    this->appPath = QUrl(baseUrl).path();
}

void RestManager::sendGet(QString path, StringHash params, StringHash headers)
{
    QNetworkRequest request = setupRequest();
    QUrl requestUrl;
    QUrlQuery query;

    requestUrl.setUrl(path.prepend(baseUrl));

    if (!params.isEmpty()) {
        StringHashIterator parameter(params);

        while(parameter.hasNext()) {
            parameter.next();

            query.addQueryItem(parameter.key(), parameter.value());
        }
    }

    requestUrl.setQuery(query);

    request.setUrl(requestUrl);
    request.setRawHeader("User-Agent", userAgent);

    if (!headers.isEmpty()) {
        StringHashIterator header(params);

        while(header.hasNext()) {
            header.next();

            request.setRawHeader(header.key().toLatin1(), header.value().toLatin1());
        }
    }

    get(request);
}

void RestManager::sendPost(QString path, StringHash params, bool isPut)
{
    QNetworkRequest request = setupRequest();
    QUrl paramsUrl;
    QUrlQuery query;

    path.prepend(baseUrl);

    request.setUrl(path);
    request.setRawHeader("User-Agent", userAgent);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    if (!params.isEmpty()) {
        StringHashIterator parameter(params);

        while(parameter.hasNext()) {
            parameter.next();

            query.addQueryItem(parameter.key(), parameter.value());
        }
    }

    paramsUrl.setQuery(query);

    if (!isPut) {
        post(request, paramsUrl.query().toUtf8());
    } else {
        put(request, paramsUrl.query().toUtf8());
    }
}

void RestManager::sendPut(QString path, StringHash params)
{
    sendPost(path, params, true);
}

void RestManager::sendDelete(QString url)
{
    QNetworkRequest request = setupRequest();

    url.prepend(baseUrl);

    request.setUrl(url);
    request.setRawHeader("User-Agent", userAgent);

    deleteResource(request);
}

void RestManager::message(QWidget *parent, QString title, QByteArray data, bool isWarning)
{
    QVariantMap response = QJsonDocument::fromJson(data).object().toVariantMap();
    QString message = response["message"].toString();

    message =  (message.isEmpty() ? "Tidak ada pesan." : message);

    if (!isWarning) QMessageBox::information(parent, title, message);
    else QMessageBox::warning(parent, title, message);
}
