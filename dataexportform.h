#ifndef DATAEXPORTFORM_H
#define DATAEXPORTFORM_H

#include <QWidget>
#include <QMap>
#include <QPointer>

#include "types.h"
#include "restmanager.h"
#include "dataexportmodel.h"

namespace Ui {
    class DataExportForm;
}

class DataExportForm : public QWidget
{
    Q_OBJECT

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

    void on_retrieveResult_clicked();
    void on_exportResult_clicked();

public:
    explicit DataExportForm(StringHash userInfo, QWidget *parent = 0);
    ~DataExportForm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::DataExportForm *ui;

    StringHash userInfo;
    QPointer<DataExportModel> dataExportModel;

    void setupOkFileTab();
    void setupRecurringTab();
    void setupTableHeaders();
    void toggleRetrieveProgress(bool toggle = true);
    QString getExportPath();
    StringHash getExportParams();
};

#endif // DATAEXPORTFORM_H
