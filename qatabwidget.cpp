#include "qatabwidget.h"
#include "qadetailform.h"

#include <QLabel>
#include <QVBoxLayout>

QATabWidget::QATabWidget(StringHash userInfo, QWidget *parent) :
    userInfo(userInfo),
    QWidget(parent)
{
    setupSearchForm();

    connect(searchForm, SIGNAL(polisTableClicked(QString,QString)), SLOT(onPolisTableClicked(QString,QString)));
}

void QATabWidget::setupSearchForm()
{
    searchForm = new QASearchForm;

    tabWidget = new SearchDetailTab;
    tabWidget->addSearchTab(searchForm, "Polis");

    setLayout(new QVBoxLayout);
    layout()->addWidget(tabWidget);
}

void QATabWidget::onPolisTableClicked(QString customerId, QString customerCode)
{
    int tabIndex = tabWidget->indexOfLabel(customerCode);
    if (tabIndex != -1) {
        tabWidget->showDetailTab(tabIndex);

        return;
    }

    tabWidget->addDetailTab(new QADetailForm(customerId), customerCode);
}
