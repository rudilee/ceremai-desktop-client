#include "cticlient.h"

#include <QHostAddress>
#include <QTimer>

CtiClient *CtiClient::instance = NULL;

CtiClient::CtiClient(QObject *parent) :
    QTcpSocket(parent)
{
    connect(this, SIGNAL(connected()), SLOT(onConnected()));
    connect(this, SIGNAL(disconnected()), SLOT(onDisconnected()));
    connect(this, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(onError(QAbstractSocket::SocketError)));
    connect(this, SIGNAL(readyRead()), SLOT(onReadyRead()));

    tryConnectToHost();
}

CtiClient *CtiClient::getInstance()
{
    if (instance == NULL) {
        instance = new CtiClient;
    }

    return instance;
}

void CtiClient::tryConnectToHost()
{
    if (state() == QAbstractSocket::UnconnectedState) {
        emit feedbackReceived("Try to connecting...", false);

        connectToHost(QHostAddress::LocalHost, 18000);
    }
}

void CtiClient::onConnected()
{
    emit feedbackReceived("Connected...", false);
}

void CtiClient::onDisconnected()
{
    emit feedbackReceived("Disconnected unexpectedly, will try to reconnect in 15 seconds.", true);

    QTimer::singleShot(15000, this, SLOT(tryConnectToHost()));
}

void CtiClient::onError(QAbstractSocket::SocketError socketError)
{
    Q_UNUSED(socketError)

    emit feedbackReceived("Socket error, will try to reconnect in 15 seconds.", true);

    QTimer::singleShot(15000, this, SLOT(tryConnectToHost()));
}

void CtiClient::onReadyRead()
{
    if (canReadLine()) {
        QString feedback = readAll();

        if (feedback.startsWith("ERR")) {
            emit feedbackReceived(feedback.replace("ERR", ""), true);
        } else {
            emit feedbackReceived(feedback.replace("FEEDBACK", ""), false);
        }
    }
}

bool CtiClient::callPhoneNumber(QString number, QString customerId, QString campaign)
{
    if (!number.isEmpty()) {
        if (state() == QAbstractSocket::ConnectedState) {
            if (write(QString("DIAL|%1|%2|%3\r\n").arg(number, customerId, campaign).toLatin1().data()) != -1) return true;
        }
    }

    return false;
}

void CtiClient::hangupPhone()
{
    if (state() == QAbstractSocket::ConnectedState) {
        write("HANGUP\r\n");
    }
}
