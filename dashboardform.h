#ifndef DASHBOARDFORM_H
#define DASHBOARDFORM_H

#include <QWidget>

namespace Ui {
    class DashboardForm;
}

class DashboardForm : public QWidget
{
    Q_OBJECT

public:
    explicit DashboardForm(QWidget *parent = 0);
    ~DashboardForm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::DashboardForm *ui;
};

#endif // DASHBOARDFORM_H
