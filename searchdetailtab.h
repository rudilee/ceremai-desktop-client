#ifndef SEARCHDETAILTAB_H
#define SEARCHDETAILTAB_H

#include <QTabWidget>
#include <QSignalMapper>

class SearchDetailTab : public QTabWidget
{
    Q_OBJECT

private slots:
    void onTabClosing(QWidget *widget);

public:
    explicit SearchDetailTab(QWidget *parent = 0);
    ~SearchDetailTab();

    void closeEvent(QCloseEvent *event);

    void addSearchTab(QWidget *widget, QString label = "");
    void addDetailTab(QWidget *widget, QString label);
    int indexOfLabel(QString label);
    void showDetailTab(int index);

private:
    QSignalMapper *closeMapper;
};

#endif // SEARCHDETAILTAB_H
