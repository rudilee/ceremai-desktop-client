#include "reportform.h"
#include "ui_reportform.h"

#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonArray>

ReportForm::ReportForm(StringHash userInfo, QWidget *parent) :
    userInfo(userInfo),
    QWidget(parent),
    ui(new Ui::ReportForm)
{
    ui->setupUi(this);

    setupReportForm();
    setupCallTrackingTab();
    toggleSearchProgress(false);

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestReplyReceived(int,QString,QString, Http::Method,QByteArray)));
}

ReportForm::~ReportForm()
{
    delete ui;
}

void ReportForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void ReportForm::setupReportForm()
{
    RestManager::getInstance()->sendGet("campaign");
}

void ReportForm::setupCallTrackingTab()
{
    QDate currentDate = QDate::currentDate();
    int year = currentDate.year(),
        month = currentDate.month();

    ui->callTrackingFrom->setDate(QDate(year, month, 1));
    ui->callTrackingTo->setDate(QDate(year, month, currentDate.daysInMonth()));
}

void ReportForm::setupCampaignSelect(QByteArray data)
{
    QVariantList campaigns = QJsonDocument::fromJson(data).array().toVariantList();

    ui->campaignSelect->addItem("", 0);

    foreach (QVariant campaign, campaigns) {
        QVariantMap campaignFields = campaign.toMap();

        ui->campaignSelect->addItem(campaignFields["name"].toString(), campaignFields["id"]);
    }
}

void ReportForm::toggleSearchProgress(bool toggle)
{
    ui->reportProgress->setMaximum(toggle ? 0 : 1);
    ui->reportProgress->setValue(toggle ? -1 : 1);
    ui->reportProgress->setVisible(toggle);
}

StringHash ReportForm::getTrackingParams()
{
    StringHash params = StringHash();

    params["campaign_id"] = ui->campaignSelect->itemData(ui->campaignSelect->currentIndex()).toString();
    params["called_from"] = ui->callTrackingFrom->date().toString("yyyy-MM-dd");
    params["called_to"] = ui->callTrackingTo->date().toString("yyyy-MM-dd");

    return params;
}

void ReportForm::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (code == 200) {
        if (path == "campaign") {
            setupCampaignSelect(data);
        } else if (path == "report/call_tracking") {
            toggleSearchProgress(false);

//            scriptViewer->clearPagesView();
//            scriptViewer->renderPages(data);
        } else if (path == "report/excel/call_tracking") {
            QString filename = QFileDialog::getSaveFileName(
                        this, "Save Call Tracking",
                        QString("%1/CALL_TRACKING_%2_%3.xls").arg(
                            QDir::homePath(),
                            ui->campaignSelect->currentText(),
                            QDateTime::currentDateTime().toString("yyyyMMddhhmmss")
                            ),
                        "Microsoft Excel 5.0 (*.xls)"
                        );

            if (filename.isEmpty()) return;

            QFile file(filename);

            if (!file.open(QIODevice::WriteOnly)) {
                QMessageBox::critical(this, "Save Error", "Membuka file untuk menyimpan gagal");

                return;
            }

            if(file.write(data) == -1) {
                QMessageBox::critical(this, "Save Error", "Menyimpan data ke file tujuan gagal");
            }
        }
    }
}

void ReportForm::on_showReport_clicked()
{
    if (ui->campaignSelect->currentText().isEmpty()) return;

    toggleSearchProgress();

    RestManager::getInstance()->sendGet("report/call_tracking", getTrackingParams());
}

void ReportForm::on_exportReport_clicked()
{
    if (ui->campaignSelect->currentText().isEmpty()) return;

    RestManager::getInstance()->sendGet("report/excel/call_tracking", getTrackingParams());
}
