#ifndef SEARCHMODEL_H
#define SEARCHMODEL_H

#include <QAbstractTableModel>
#include <QStringList>

#include "restmanager.h"

class SearchModel : public QAbstractTableModel
{
    Q_OBJECT

signals:
    void columnSorting(QString order, QString sort);

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

public:
    explicit SearchModel(QString modelPath, QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    void sort(int column, Qt::SortOrder order);

    void setHeaders(QStringList headers);
    void setFields(QStringList fields);
    void setParseRow(QStringList (*parseRow)(QVariantMap));
    void setDataRole(QVariant (*foregroundRole)(QStringList), QVariant (*textAlignmentRole)(int));
    QString getColumnValue(QModelIndex index, int position = 0);

private:
    RestManager *restManager;
    QStringList (*parseRow)(QVariantMap);
    QVariant (*foregroundRole)(QStringList);
    QVariant (*textAlignmentRole)(int);
    QString modelPath;
    QStringList headers;
    QStringList fields;
    QList<QStringList> searchResult;
    StringHash searchParams;
    int pageNumber;
    int sequence;
    int column;
    Qt::SortOrder order;
};

#endif // SEARCHMODEL_H
