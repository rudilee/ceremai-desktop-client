#ifndef CTICLIENT_H
#define CTICLIENT_H

#include <QTcpSocket>

class CtiClient : public QTcpSocket
{
    Q_OBJECT
    Q_DISABLE_COPY(CtiClient)

signals:
    void feedbackReceived(QString feedback, bool isError);

private slots:
    void tryConnectToHost();
    void onConnected();
    void onDisconnected();
    void onError(QAbstractSocket::SocketError socketError);
    void onReadyRead();

public:
    static CtiClient *getInstance();

    bool callPhoneNumber(QString number, QString customerId, QString campaign);
    void hangupPhone();

private:
    static CtiClient *instance;

    explicit CtiClient(QObject *parent = 0);
};

#endif // CTICLIENT_H
