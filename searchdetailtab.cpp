#include "searchdetailtab.h"

#include <QToolButton>
#include <QTabBar>
#include <QLabel>
#include <QCloseEvent>

SearchDetailTab::SearchDetailTab(QWidget *parent) :
    QTabWidget(parent)
{
    closeMapper = new QSignalMapper;

    connect(closeMapper, SIGNAL(mapped(QWidget*)), SLOT(onTabClosing(QWidget*)));
}

SearchDetailTab::~SearchDetailTab()
{
    delete closeMapper;
}

void SearchDetailTab::closeEvent(QCloseEvent *event)
{
    QWidget *widget;

    setCurrentIndex(count() - 1);

//    widget = this->widget(count() - 1);
//    if (!widget->close()) {
//        event->ignore();

//        return;
//    }

    for (int index = count() - 1; index >= 0; index--) {
        widget = currentWidget();
        if (!widget->close()) {
            event->ignore();

            return;
        }

        delete widget;
    }
}

void SearchDetailTab::addSearchTab(QWidget *widget, QString label)
{
    QLabel *icon = new QLabel;
    icon->setPixmap(QPixmap(":button/search"));
    icon->setMinimumHeight(22);

    int newIndex = addTab(widget, label.prepend("Search "));

    tabBar()->setTabButton(newIndex, QTabBar::LeftSide, icon);
}

void SearchDetailTab::addDetailTab(QWidget *widget, QString label)
{
    int index = indexOf(widget);
    if (index == -1) {
        QToolButton *close = new QToolButton;
        close->setAutoRaise(true);
        close->setIcon(QIcon(":button/close tab"));
        close->setFixedSize(20, 20);

        index = addTab(widget, label);

        tabBar()->setTabButton(index, QTabBar::RightSide, close);

        closeMapper->setMapping(close, widget);

        connect(close, SIGNAL(clicked()), closeMapper, SLOT(map()));
    }

    setCurrentIndex(index);
}

int SearchDetailTab::indexOfLabel(QString label)
{
    for (int index = 1; index < count(); index++) {
        if (tabText(index) == label) {
            return index;
        }
    }

    return -1;
}

void SearchDetailTab::showDetailTab(int index)
{
    setCurrentIndex(index);
}

void SearchDetailTab::onTabClosing(QWidget *widget)
{
    int index = indexOf(widget);
    if (index != -1) {
        if (widget->close()) {
            removeTab(index);

            delete widget;
        } else {
            setCurrentIndex(index);
        }
    }
}
