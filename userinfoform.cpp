#include "userinfoform.h"
#include "ui_userinfoform.h"

UserInfoForm::UserInfoForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserInfoForm)
{
    ui->setupUi(this);
}

UserInfoForm::~UserInfoForm()
{
    delete ui;
}

void UserInfoForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void UserInfoForm::setUserInfo(QString username, QString fullname, QString role, QString roleName)
{
    ui->userLabel->setText(QString(" %1 - %2").arg(username, fullname));
    ui->roleIcon->setPixmap(QPixmap(QString(":/combo/").append(role)));
    ui->roleLabel->setText(QString(" %1").arg(roleName));
}
