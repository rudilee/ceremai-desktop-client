#ifndef CALLSTATUS_H
#define CALLSTATUS_H

#include <QObject>
#include <QMap>

#include "restmanager.h"

class CallStatus : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(CallStatus)

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

public:
    static CallStatus *getInstance();

    QMap<int, QString> getCallStatuses();
    QMap<int, QString> getCallReasons(int callStatusId);

private:
    static CallStatus *instance;

    QMap<int, QString> callStatuses;
    QMap<int, QString> callReasons;
    QMap<int, int> callReasonStatus;

    explicit CallStatus(QObject *parent = 0);

    void setupCallStatus(QByteArray data);
    void setupCallReason(QByteArray data);
};

#endif // CALLSTATUS_H
