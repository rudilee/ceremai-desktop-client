#ifndef DATAEXPORTMODEL_H
#define DATAEXPORTMODEL_H

#include <QAbstractTableModel>
#include <QStringList>

#include "restmanager.h"

class DataExportModel : public QAbstractTableModel
{
    Q_OBJECT

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

public:
    explicit DataExportModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    void setHeaders(QStringList headers);
    void setRetrievePath(QString retrievePath);

private:
    QStringList headers;
    QList<QStringList> retrieveResult;
    QString retrievePath;
};

#endif // DATAEXPORTMODEL_H
