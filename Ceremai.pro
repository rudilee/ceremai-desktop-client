#-------------------------------------------------
#
# Project created by QtCreator 2012-05-27T22:48:16
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Ceremai
TEMPLATE = app

SOURCES += main.cpp\
    mainwindow.cpp \
    logindialog.cpp \
    restmanager.cpp \
    dashboardform.cpp \
    cticlient.cpp \
    distributeform.cpp \
    reportform.cpp \
    qasearchmodel.cpp \
    searchdetailtab.cpp \
    qasearchform.cpp \
    qadetailform.cpp \
    manageuserform.cpp \
    changepasswordform.cpp \
    dataexportform.cpp \
    dataimportform.cpp \
    dataexportmodel.cpp \
    userinfoform.cpp \
    customerdetailform.cpp \
    customersearchform.cpp \
    customersearchmodel.cpp \
    customertabwidget.cpp \
    qatabwidget.cpp \
    callstatus.cpp \
    datepickercalendar.cpp \
    datepickerinput.cpp \
    qacallscoringform.cpp \
    customermemodialog.cpp \
    searchsortmodel.cpp \
    searchmodel.cpp

HEADERS += mainwindow.h \
    logindialog.h \
    restmanager.h \
    dashboardform.h \
    cticlient.h \
    distributeform.h \
    reportform.h \
    qasearchmodel.h \
    searchdetailtab.h \
    qasearchform.h \
    qadetailform.h \
    manageuserform.h \
    changepasswordform.h \
    dataexportform.h \
    dataimportform.h \
    dataexportmodel.h \
    userinfoform.h \
    customerdetailform.h \
    customersearchform.h \
    customersearchmodel.h \
    customertabwidget.h \
    qatabwidget.h \
    callstatus.h \
    datepickercalendar.h \
    datepickerinput.h \
    qacallscoringform.h \
    customermemodialog.h \
    searchmodel.h \
    searchsortmodel.h \
    types.h

FORMS   += mainwindow.ui \
    logindialog.ui \
    dashboardform.ui \
    distributeform.ui \
    reportform.ui \
    qasearchform.ui \
    qadetailform.ui \
    manageuserform.ui \
    changepasswordform.ui \
    dataexportform.ui \
    dataimportform.ui \
    userinfoform.ui \
    customersearchform.ui \
    customerdetailform.ui \
    datepickercalendar.ui \
    datepickerinput.ui \
    qacallscoringform.ui \
    customermemodialog.ui

win32 {
    LIBS += -LE:/Hacks/C++/src/qtsingleapplication/lib -lQtSolutions_SingleApplication-head -LQtSolutions_SingleApplication-head.dll

    INCLUDEPATH += E:/Hacks/C++/src/qtsingleapplication/src

    RC_FILE = appicon.rc
}

linux-g++ {
    INCLUDEPATH += /home/rudilee/Hacks/C++/src/qtsingleapplication/src\

    LIBS += -L/usr/local/lib -lQtSolutions_SingleApplication-head
}

RESOURCES += icons.qrc
