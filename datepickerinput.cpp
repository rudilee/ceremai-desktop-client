#include "datepickerinput.h"
#include "ui_datepickerinput.h"

DatePickerInput::DatePickerInput(QWidget *parent) :
    QLineEdit(parent),
    ui(new Ui::DatePickerInput)
{
    ui->setupUi(this);

//    calendarPopup = new QCalendarWidget;
    calendarPopup.setParent((QWidget *) parent->parent()->parent());
    calendarPopup.setWindowFlags(Qt::Popup);
    calendarPopup.hide();
}

DatePickerInput::~DatePickerInput()
{
    delete ui;
}

void DatePickerInput::changeEvent(QEvent *e)
{
    QLineEdit::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void DatePickerInput::focusInEvent(QFocusEvent *e)
{
    QLineEdit::focusInEvent(e);

    calendarPopup.show();
}

void DatePickerInput::focusOutEvent(QFocusEvent *e)
{
    QLineEdit::focusOutEvent(e);

    calendarPopup.hide();
}
