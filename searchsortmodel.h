#ifndef SEARCHSORTMODEL_H
#define SEARCHSORTMODEL_H

#include <QSortFilterProxyModel>

class SearchSortModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit SearchSortModel(QObject *parent = 0);

    void sort(int column, Qt::SortOrder order);
};

#endif // SEARCHSORTMODEL_H
