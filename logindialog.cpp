#include "logindialog.h"
#include "ui_logindialog.h"
#include "types.h"

#include <QPointer>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);

    settings.setParent(this);

    QString appTitle = QString("%1 %2").arg(QCoreApplication::applicationName(), QCoreApplication::applicationVersion());

    setWindowTitle(QString("%1 - Login").arg(appTitle));
    setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | /*Qt::WindowTitleHint |*/ Qt::WindowStaysOnTopHint);

    ui->applicationLabel->setText(appTitle);

    setupServersList();

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString, Http::Method,QByteArray)),
            SLOT(onRestReplyReceived(int,QString,QString, Http::Method,QByteArray)));
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void LoginDialog::setupServersList()
{
    QStringList servers = settings.value("serverslist", "").toStringList();

    if (!servers.isEmpty()) {
        foreach (QString server, servers) {
            ui->serversList->insertItem(0, server);
        }
    }

    if (ui->serversList->count() > 0) {
        ui->serversList->removeItem(ui->serversList->findText(""));
        ui->serversList->setCurrentIndex(0);
    }
}

void LoginDialog::clearFields()
{
    ui->usernameEdit->clear();
    ui->usernameEdit->setFocus();
    ui->passwordEdit->clear();
}

void LoginDialog::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (path == "authentication/login") {
        if (code == 200) {
            QString serverUrl = ui->serversList->currentText();

            if (ui->serversList->findText(serverUrl) == -1) {
                ui->serversList->insertItem(0, serverUrl);

                QStringList addedServersList = settings.value("serverslist", "").toStringList();
                addedServersList.insert(0, serverUrl);

                settings.setValue("serverslist", addedServersList);
            }

            clearFields();
            accept();
        } else {
            QVariantMap message = QJsonDocument::fromJson(data).object().toVariantMap();qDebug() << "Kampretos:" << message;
            QString messageText = message["message"].toString();

            QMessageBox::warning(this, "Login Failed", messageText.isEmpty() ? "Tidak dapat menghubungi server" : messageText);

            clearFields();
        }
    }
}

void LoginDialog::on_login_clicked()
{
    QString serverUrl = ui->serversList->currentText(), username = ui->usernameEdit->text(),
            password = ui->passwordEdit->text();

    StringHash params;
    params.insert("username", username);
    params.insert("password", password);

    if (serverUrl.isEmpty() || username.isEmpty() || password.isEmpty()) {
        QMessageBox::warning(this, "Login failed", "Username atau password tidak boleh kosong");

        return;
    }

    QPointer<RestManager> restManager = RestManager::getInstance();

    restManager->setBaseUrl(serverUrl);
    restManager->sendGet("authentication/login", params);
}

void LoginDialog::on_cancel_clicked()
{
    close();
}
