#ifndef DISTRIBUTEFORM_H
#define DISTRIBUTEFORM_H

#include <QWidget>
#include <QMap>

#include "types.h"
#include "restmanager.h"

namespace Ui {
    class DistributeForm;
}

class DistributeForm : public QWidget
{
    Q_OBJECT

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

    void on_campaignCombo_activated(int index);
    void on_transferData_clicked();
    void on_withdrawData_clicked();

public:
    explicit DistributeForm(StringHash userInfo, QWidget *parent = 0);
    ~DistributeForm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::DistributeForm *ui;

    StringHash userInfo;
    QMap<int, QStringList> userRole;

    void setupUsersTable();
    void refreshDistributeForm();
    void setupUserRole(QByteArray data);
    void setupCampaignSelect(QByteArray data);
    void populateUsersTable(QByteArray data);
};

#endif // DISTRIBUTEFORM_H
