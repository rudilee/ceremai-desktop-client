#include "dashboardform.h"
#include "ui_dashboardform.h"

DashboardForm::DashboardForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DashboardForm)
{
    ui->setupUi(this);
}

DashboardForm::~DashboardForm()
{
    delete ui;
}

void DashboardForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
