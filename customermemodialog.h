#ifndef CUSTOMERMEMODIALOG_H
#define CUSTOMERMEMODIALOG_H

#include <QDialog>

namespace Ui {
class CustomerMemoDialog;
}

class CustomerMemoDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit CustomerMemoDialog(QWidget *parent = 0);
    ~CustomerMemoDialog();

    void setMemoContent(QString memoContent);
    
protected:
    void changeEvent(QEvent *e);
    
private:
    Ui::CustomerMemoDialog *ui;
};

#endif // CUSTOMERMEMODIALOG_H
