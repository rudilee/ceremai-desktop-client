#ifndef CHANGEPASSWORDFORM_H
#define CHANGEPASSWORDFORM_H

#include <QWidget>

#include "restmanager.h"

namespace Ui {
    class ChangePasswordForm;
}

class ChangePasswordForm : public QWidget
{
    Q_OBJECT

private slots:
    void onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data);

    void on_changePassword_clicked();

public:
    explicit ChangePasswordForm(QWidget *parent = 0);
    ~ChangePasswordForm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::ChangePasswordForm *ui;
};

#endif // CHANGEPASSWORDFORM_H
