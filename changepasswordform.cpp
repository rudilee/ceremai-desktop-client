#include "changepasswordform.h"
#include "ui_changepasswordform.h"

#include <QMessageBox>

ChangePasswordForm::ChangePasswordForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChangePasswordForm)
{
    ui->setupUi(this);

    connect(RestManager::getInstance(), SIGNAL(replyReceived(int,QString,QString,Http::Method,QByteArray)),
            SLOT(onRestReplyReceived(int,QString,QString,Http::Method,QByteArray)));
}

ChangePasswordForm::~ChangePasswordForm()
{
    delete ui;
}

void ChangePasswordForm::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void ChangePasswordForm::onRestReplyReceived(int code, QString path, QString contentType, Http::Method method, QByteArray data)
{
    if (contentType != "application/json") return;

    if (code == 200) {
        if (path == "user/password") {
            RestManager::message(this, data, "Change Password");

            ui->oldPassword->clear();
            ui->password->clear();
            ui->passwordVerify->clear();
        }
    }
}

void ChangePasswordForm::on_changePassword_clicked()
{
    QString oldPassword = ui->oldPassword->text(),
            password = ui->password->text(),
            passwordVerify = ui->passwordVerify->text();

    StringHash params = StringHash();

    if (oldPassword.isEmpty() || password.isEmpty() || passwordVerify.isEmpty()) {
        QMessageBox::warning(this, "Empty Fields", "Field mandatori tidak boleh dikosongkan");

        return;
    }

    params.insert("old_password", oldPassword);
    params.insert("password", password);
    params.insert("password_verify", passwordVerify);

    RestManager::getInstance()->sendPut("user/password", params);
}
